import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, RouterEvent } from '@angular/router';
import { iif, Observable, of, Subject } from 'rxjs';
import { concatMap, filter, finalize, map, switchMap, takeUntil } from 'rxjs/operators';
import { IDocument } from '@aiur/core/models/IDocument';
import { DependencyTreeType } from '@aiur/core/models/DependencyTreeType';
import { DependencyTreeStatus } from '@aiur/core/models/DependencyTreeStatus';
import { IDependencyTreeMetadata } from '@aiur/core/models/IDependencyTreeMetadata';
import { User } from '@aiur/core/models/User';
import { UserService } from '@aiur/core/services/user.service';
import { DocumentsService } from '@aiur/core/services/documents.service';
import { IDndTreeData } from '../../models/IDndTreeData';
import { SubmitDependencyTreeType, TreeService } from '../../services/tree.service';
import { TreeEditorService } from '../../services/tree-editor.service';
import { TreeEditorActions } from '../../models/TreeEditorActions';
import { DndTreeService } from '../../services/dnd-tree.service';
import { IDependencyTree } from '@aiur/core/models/IDependencyTree';
import { MatDialog } from '@angular/material/dialog';
import { TreeSuccessfulUpdatePopupComponent } from '@aiur/modules/widgets/tree-editor/components/tree-successful-update-popup/tree-successful-update-popup.component';
import { ITreeResponse } from '@aiur/modules/widgets/tree-editor/models/ITreeResponse';


@Component({
  selector: 'aif-tree-editor-layout',
  templateUrl: './tree-editor-layout.component.html',
  styleUrls: ['./tree-editor-layout.component.scss']
})
export class TreeEditorLayoutComponent implements OnInit, OnDestroy {
  @Input() public documentId: string;
  public isLoading: boolean;
  public document: IDocument;
  public dndTreeData: IDndTreeData;
  public isTreeEditable: boolean;
  public documentDetails: Omit<IDndTreeData, ('children' | '_children')>;
  public buttonActions: typeof TreeEditorActions;
  private _dependencyTreeStatus: DependencyTreeStatus;
  private _unsubscribeAll$: Subject<void>;

  constructor(private _treeService: TreeService,
              private _userService: UserService,
              private _dialog: MatDialog,
              private _router: Router,
              private _route: ActivatedRoute,
              private _documentsService: DocumentsService,
              private _dndTreeService: DndTreeService,
              private _treeEditor: TreeEditorService) {

    this.isTreeEditable = false;
    this.buttonActions = TreeEditorActions;
    this._unsubscribeAll$ = new Subject<void>();
  }

  ngOnInit(): void {
    this.isLoading = true;

    this._router.events
      .pipe(
        filter((event: RouterEvent) => (event instanceof NavigationEnd)),
        takeUntil(this._unsubscribeAll$)
      ).subscribe((event: NavigationEnd) => this._updateTree(event.url));

    this._treeEditor.actions$.pipe(
      takeUntil(this._unsubscribeAll$)
    ).subscribe((action: TreeEditorActions) => this._editorActionsSubscriber(action));

    this._documentsService.getById(this.documentId)
      .subscribe({
        next: (document: IDocument) => this.document = document,
        complete: () => this._updateTree(this._router.url)
      });
  }

  ngOnDestroy(): void {
    this._unsubscribeAll$.next();
    this._unsubscribeAll$.complete();
    this._treeEditor.dndTree.event.unsubscribeAll();
  }

  closeDocumentDetails(): void {
    this.documentDetails = void (0);
  }

  private _onLabelTreeSubscriber(data: IDndTreeData): void {
    delete data.children;
    delete data._children;

    this.documentDetails = {...data};
  }

  private _resetTree(): void {
    this._showOriginTree();
  }

  private _showOriginTree() {
    this.isLoading = true;
    this.dndTreeData = this._getOriginalTreeData();
    this._afterSettingUpTreeData();
  }

  private _showMyEditTree(): void {
    this.isLoading = true;
    this._getMyTreeData()
      .pipe(finalize(() => this._afterSettingUpTreeData()))
      .subscribe((data: IDndTreeData) => this.dndTreeData = data);
  }

  private _showCompareTree(): void {
    this.isLoading = true;

    // @TODO: tmp solution
    const isMyTreeCompareTMP: boolean = this._router.url.endsWith('reviewed/compare');
    const treeId = this._route.snapshot.paramMap.get('treeId');

    this._userService.info$.pipe(
      map(({user_id}: User) => user_id),
      map((user_id: number) => {
        return isMyTreeCompareTMP
          ? {id: treeId}
          : this.document.trees.filter(({ type, owner_id}: IDependencyTreeMetadata) => {
            return type === DependencyTreeType.REVIEW && owner_id === user_id
          }).pop();
      }),
      switchMap(({id}: IDependencyTreeMetadata) => this._treeService.compareTree(id)),
      finalize(() => this._afterSettingUpTreeData())
    ).subscribe((dependency_tree: IDependencyTree) => {
      this.dndTreeData = this._dndTreeService.generateDndTreeDataStructure(dependency_tree);
    });
  }

  private _afterSettingUpTreeData(): void {
    this._treeEditor.dndTree.event.unsubscribeAll();
    setTimeout(() => {
      this.isLoading = false;

      this._treeEditor.dndTree
        .event
        .subscribe('HE:onLabelClick', (data: IDndTreeData) => this._onLabelTreeSubscriber(data));

      this._treeEditor.dndTree
        .event
        .subscribe('HE:data', (data: IDndTreeData) => this._onDndTreeDataSubscriber(data));
    }, 300);
  }

  private _getOriginalTreeData(): IDndTreeData {
    const originalTree: IDndTreeData = this.document
      .trees
      .filter(({type}: IDependencyTreeMetadata) => type === DependencyTreeType.SUBMISSION)
      .map(({dependency_tree}: IDependencyTreeMetadata) => {
        return this._dndTreeService.generateDndTreeDataStructure(dependency_tree);
      })
      .pop();

    return {...originalTree};
  }

  private _getMyTreeData(): Observable<IDndTreeData> {
    return this._userService.info$.pipe(
      map(({user_id}: User) => user_id),
      map((user_id: number) => {
        let treeId: number = this.document
          .trees
          .filter(({type, owner_id}: IDependencyTreeMetadata) => type === DependencyTreeType.REVIEW && owner_id === user_id)
          .map(({id}: IDependencyTreeMetadata) => id)
          .pop();

        if (!treeId) {
          treeId = this.document
            .trees
            .filter(({type}: IDependencyTreeMetadata) => type === DependencyTreeType.SUBMISSION)
            .map(({id}: IDependencyTreeMetadata) => id)
            .pop();
        }

        return treeId;
      }),
      switchMap((treeId: number) => this._treeService.getById(treeId)),
      map(({dependency_tree}: ITreeResponse) => this._dndTreeService.generateDndTreeDataStructure(dependency_tree))
    );
  }

  private _saveEditedTreeData(): void {
    this._dependencyTreeStatus = DependencyTreeStatus.IN_PROGRESS;
    this._treeEditor.dndTree.event.emit('HE:getData');
  }

  private _submitEditedTreeData(): void {
    this._dependencyTreeStatus = DependencyTreeStatus.SUBMITTED;
    this._treeEditor.dndTree.event.emit('HE:getData');
  }

  private _onDndTreeDataSubscriber(data: IDndTreeData) {

    const isCreate: boolean = this._router.url.endsWith('tree-validation/my');
    const isMyTreeUpdate: boolean = this._router.url.endsWith('tree/origin');

    const treeData: SubmitDependencyTreeType = {
      type: isMyTreeUpdate ? DependencyTreeType.SUBMISSION : DependencyTreeType.REVIEW,
      document_id: this.document.id,
      status: this._dependencyTreeStatus,
      owner_id: null,
      dependency_tree: this._dndTreeService.generateDependencyTreeDataStructure(data)
    };

    of(treeData).pipe(
      concatMap(
        (treeData: SubmitDependencyTreeType) => this._userService.info$.pipe(
          map(({user_id}: User) => Object.assign(treeData, {owner_id: user_id}))
        )
      ),
      concatMap((treeData: SubmitDependencyTreeType) => iif(
        () => isCreate,
        this._treeService.createReview(treeData),
        this._userService.info$.pipe(
          map(({user_id}: User) => user_id),
          map((user_id: number) => {
            return this.document
              .trees
              .filter(({type, owner_id}: IDependencyTreeMetadata) => {
                return isMyTreeUpdate ? owner_id === user_id : (type === DependencyTreeType.REVIEW && owner_id === user_id)
              }).pop();
          }),
          concatMap(({id}: IDependencyTreeMetadata) => this._treeService.updateReview(id, treeData))
        )
      ))
    ).subscribe(({document_id}: IDependencyTreeMetadata) => {
      isCreate
        ? this._router.navigate(['document', document_id, 'tree-review', 'my'])
        : this._dialog.open(TreeSuccessfulUpdatePopupComponent);
    });
  }

  private _editorActionsSubscriber(action: TreeEditorActions): void {
    switch (action) {
      case TreeEditorActions.RESET:
        this._resetTree();
        break;
      case TreeEditorActions.SAVE:
        this._saveEditedTreeData();
        break;
      case TreeEditorActions.SUBMIT:
        this._submitEditedTreeData();
        break;
      case TreeEditorActions.EXPAND_ALL:
        this._treeEditor.dndTree.event.emit('HE:expandAll');
        break;
      case TreeEditorActions.COLLAPSE_ALL:
        this._treeEditor.dndTree.event.emit('HE:collapseAll');
        break;
      case TreeEditorActions.SHOW_ORIGIN_TREE:
        this._showOriginTree();
        break;
      case TreeEditorActions.SHOW_MY_EDIT_TREE:
        this._showMyEditTree();
        break;
      case TreeEditorActions.SHOW_COMPARISON_TREE:
        alert('TreeEditorActions.SHOW_COMPARISON_TREE');
        break;
    }
  }

  private _updateTree(url: string) {
    this.closeDocumentDetails();

    switch (true) {
      case url.endsWith('origin'):
        // @TODO: fix condition - "my trees" -> edit -> /document/111/tree/origin - should be editable
        this.isTreeEditable = url.endsWith('tree/origin') || false;
        this._showOriginTree();
        break;

      case url.endsWith('my'):
        this.isTreeEditable = true;
        this._showMyEditTree();
        break;

      case url.endsWith('compare'):
        this.isTreeEditable = false;
        this._showCompareTree();
        break;
    }
  }
}
