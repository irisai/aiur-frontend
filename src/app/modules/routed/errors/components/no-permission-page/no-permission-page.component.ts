import { Component } from '@angular/core';
import { ErrorsPageCommonService } from '@aiur/modules/routed/errors/services/errors-page-common.service';

@Component({
  selector: 'aif-no-permission-page',
  templateUrl: './no-permission-page.component.html',
  styleUrls: ['./no-permission-page.component.scss']
})
export class NoPermissionPageComponent {
  constructor(public errorsPageService: ErrorsPageCommonService) { }
}
