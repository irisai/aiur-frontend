import { Injectable } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { AES } from 'crypto-js';


@Injectable({
  providedIn: 'root'
})
export class CryptoService {
  private readonly host: string;

  constructor(private _platformLocation: PlatformLocation) {
    this.host = (_platformLocation['location'] as Location).host;
  }

  encrypt(key: string): { encryption: string, ISODate: string } {
    const dateNow = (Date.now()).toString().slice(0, -3);
    const ISODate = new Date(Number(dateNow + '000')).toISOString().slice(0, -5).replace('T', ' ');
    const encryption = AES.encrypt(key, this._getSecretPassphrase(dateNow)).toString();

    return {encryption, ISODate};
  }

  private _getSecretPassphrase(date: string): string {
    return `${this.host}${date}`;
  }
}
