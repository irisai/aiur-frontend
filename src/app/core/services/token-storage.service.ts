import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { CookiesStorageService } from './cookies-storage.service';
import { env } from '@config/environment';


const MAIN_TOKEN_NAME = `${env.ENV_PREFIX}jwt`;
const CLIENT_TOKEN_NAME = `${env.ENV_PREFIX}jwt-client`;
const USER_TOKEN_NAME = `${env.ENV_PREFIX}jwt-user`;
const REFRESH_TOKEN_NAME = `${env.ENV_PREFIX}jwt-refresh`;


@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {
  private _tokenName: string;
  private _jwtHelper: JwtHelperService;

  constructor(private _cookiesStorage: CookiesStorageService) {
    this._jwtHelper = new JwtHelperService();
  }

  get(): string {
    return this._cookiesStorage.get(this._tokenName);
  }

  create(token: string): void {
    const tokenExpirationTime = this._jwtHelper.getTokenExpirationDate(token);
    this._cookiesStorage.set(this._tokenName, token, tokenExpirationTime);
  }

  destroy(): void {
    this._cookiesStorage.delete(this._tokenName);
  }

  destroyAll(): void {
    [
      MAIN_TOKEN_NAME,
      CLIENT_TOKEN_NAME,
      USER_TOKEN_NAME,
      REFRESH_TOKEN_NAME
    ].forEach((tokenName) => this._cookiesStorage.delete(tokenName));
  }

  main(): TokenStorageService {
    this._tokenName = MAIN_TOKEN_NAME;
    return this;
  }

  user(): TokenStorageService {
    this._tokenName = USER_TOKEN_NAME;
    return this;
  }

  refresh(): TokenStorageService {
    this._tokenName = REFRESH_TOKEN_NAME;
    return this;
  }

  client(): TokenStorageService {
    this._tokenName = CLIENT_TOKEN_NAME;
    return this;
  }
}
