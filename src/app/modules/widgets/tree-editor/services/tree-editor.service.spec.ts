import { TestBed } from '@angular/core/testing';

import { TreeEditorService } from './tree-editor.service';

describe('TreeEditorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TreeEditorService = TestBed.get(TreeEditorService);
    expect(service).toBeTruthy();
  });
});
