import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { fuseAnimations } from '@fuse/animations';
import {DocumentsOrdering, IDocument} from '@aiur/core/models/IDocument';
import { DocumentsService } from '@aiur/core/services/documents.service';


@Component({
  selector: 'aif-my-trees-page',
  templateUrl: './my-trees-page.component.html',
  styleUrls: ['./my-trees-page.component.scss'],
  animations: fuseAnimations
})
export class MyTreesPageComponent implements OnInit {
  public documentsList$: Observable<Array<IDocument>>;
  public isLoading$: Observable<boolean>;
  public isEmpty: boolean;

  constructor(private _documents: DocumentsService) {
  }

  ngOnInit(): void {
    this.isLoading$ = this._documents.loading$;
    this.documentsList$ = this._documents.list$.pipe(
      tap((result) => this.isEmpty = typeof result !== 'undefined' ? result.length <= 0 : true),
    );

    this._documents.init({
      ordering: DocumentsOrdering.NEWEST
    });
    this._documents.load();
  }

  documentSearchChange(query: string): void {
    this._documents.searching(query);
  }

  documentOrderChange(orderingType: string): void {
    this._documents.orderBy(orderingType);
  }

  trackByFn(index: number): number {
    return index;
  }
}
