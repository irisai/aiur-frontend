import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { MyReviewsRoutingModule } from './my-reviews-routing.module';
import { MyReviewsPageComponent } from './components/my-reviews-page/my-reviews-page.component';
import { DocumentPanelModule } from '@aiur/modules/widgets/document-panel/document-panel.module';
import { NoResultModule } from '@aiur/modules/widgets/no-result/no-result.module';



@NgModule({
  imports: [
    MatIconModule,
    FuseSharedModule,
    MyReviewsRoutingModule,
    DocumentPanelModule,
    NoResultModule
  ],
  declarations: [
    MyReviewsPageComponent
  ]

})
export class MyReviewsModule { }
