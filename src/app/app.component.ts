import { Component } from '@angular/core';

@Component({
  selector: 'aiur-root',
  template: `
      <aif-container></aif-container>
  `,
  styles: [`
    :host {
        position: relative;
        display: flex;
        flex: 1 1 auto;
        width: 100%;
        height: 100%;
        min-width: 0;
    }
  `]
})
export class AppComponent {
  constructor() {}
}
