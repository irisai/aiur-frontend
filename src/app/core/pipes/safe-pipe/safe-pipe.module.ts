import { NgModule } from '@angular/core';
import { SafePipe } from './safe.pipe';


@NgModule({
  providers: [SafePipe],
  declarations: [SafePipe],
  exports: [SafePipe]
})
export class SafePipeModule {
}
