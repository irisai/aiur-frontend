import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';
import { FuseConfigService } from '@fuse/services/config.service';
import { EmailValidator } from '@aiur/core/validators/email.validator';
import { IUserTypes } from '@aiur/core/models/IUser';
import {
  UserSeniorityTypesList,
  UserExpertiseInTypesList,
  UserPositionInCompanyTypesList, UserService
} from '@aiur/core/services/user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { finalize } from 'rxjs/operators';


@Component({
  selector: 'aif-registration-page',
  templateUrl: './registration-page.component.html',
  styleUrls: ['./registration-page.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class RegistrationPageComponent implements OnInit {
  public registerForm: FormGroup;
  public userSeniorityTypesList: Array<IUserTypes>;
  public userExpertiseInTypesList: Array<IUserTypes>;
  public userPositionInCompanyTypesList: Array<IUserTypes>;
  public submitted: boolean;
  public completed: boolean;
  public userExist: boolean;
  public isServerError: boolean;

  constructor(private _fuseConfigService: FuseConfigService,
              private _formBuilder: FormBuilder,
              private _user: UserService) {

    // Configure the layout
    this._fuseConfigService.config = {
      layout: {
        navbar: {hidden: true},
        toolbar: {hidden: true},
        footer: {hidden: true},
        sidepanel: {hidden: true}
      }
    };
    this.userSeniorityTypesList = UserSeniorityTypesList;
    this.userExpertiseInTypesList = UserExpertiseInTypesList;
    this.userPositionInCompanyTypesList = UserPositionInCompanyTypesList;
    this.completed = false;
    this.userExist = false;
    this.submitted = false;
    this.isServerError = false;
  }

  ngOnInit(): void {
    this.registerForm = this._formBuilder.group({
      first_name: ['', Validators.required],
      email: ['', [Validators.required, EmailValidator]],
      position_in_company: ['', Validators.required],
      expertises: ['', Validators.required],
      seniority: [this.userSeniorityTypesList[0].type, Validators.required],
      accept_terms: [false, Validators.required]
    });
  }

  get form(): { [key: string]: AbstractControl } {
    return this.registerForm.controls;
  }

  registration(): void {
    this.submitted = true;

    this._user.registration(this.registerForm.getRawValue())
      .pipe(finalize(() => this.submitted = false))
      .subscribe(
        () => this.successRegistrationSubscriber(),
        (error) => this.errorRegistrationSubscriber(error)
      );
  }

  successRegistrationSubscriber(): void {
    this.completed = true;
  }

  errorRegistrationSubscriber(error: HttpErrorResponse): void {
    this.userExist = (error.status === 400);
    this.isServerError = (/^5\d{2}/gi).test(error.status.toString());

    if (this.userExist) {
      setTimeout(() => {
        this.userExist = false;
        this.registerForm.patchValue({acceptTerms: false});
      }, 5000);
    } else {
      console.log('else');
      this.isServerError = true;
      this.completed = true;
    }

    throw error;
  }

  mockAlert(): void {
    alert('Terms and Conditions.OPEN');
  }
}
