import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeValidationLayoutComponent } from './tree-validation-layout.component';

describe('TreeValidationLayoutComponent', () => {
  let component: TreeValidationLayoutComponent;
  let fixture: ComponentFixture<TreeValidationLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreeValidationLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeValidationLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
