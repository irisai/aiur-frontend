import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, switchMap, tap } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenStorageService } from '@aiur/core/services/token-storage.service';
import { AuthService } from '@aiur/core/services/auth.service';
import { env } from '@config/environment';


const FAILED_REQUEST_COUNT_LIMIT = 3;

enum ServerErrorText {
  INVALID_SIGNATURE = 'Invalid signature.',
  NO_PERMISSIONS = 'You do not have permission to perform this action.',
  INCORRECT_AUTHENTICATION_CREDENTIALS = 'Incorrect authentication credentials.'
}


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  private _jwtHelper: JwtHelperService;
  private readonly _responseErrorsCounter: Object;
  private _isBrowserTimeIsWrongOpen: boolean;

  constructor(private _zone: NgZone,
              private _router: Router,
              private _http: HttpClient,
              private _auth: AuthService,
              private _tokenStorage: TokenStorageService) {

    this._jwtHelper = new JwtHelperService();
    this._responseErrorsCounter = {};
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const isAPIRequest: boolean = request instanceof HttpRequest && request.url.includes(env.API_URL);

    if (!isAPIRequest) {
      return next.handle(request);
    } else {
      const mainJWT = this._tokenStorage.main().get();
      const userJWT = this._tokenStorage.user().get();
      const clientJWT = this._tokenStorage.client().get();
      const refreshJWT = this._tokenStorage.refresh().get();

      const isUserTokenExpired: boolean = !userJWT || userJWT && this._jwtHelper.isTokenExpired(userJWT);
      const isClientTokenExpired: boolean = !clientJWT || clientJWT && this._jwtHelper.isTokenExpired(clientJWT);
      const isRefreshTokenExpired: boolean = refreshJWT && this._jwtHelper.isTokenExpired(refreshJWT);
      const isUserLoginRequest: boolean = request.url.endsWith('/api/token/') && request.body && Object.keys(request.body).includes('remember_me');

      if (!this._responseErrorsCounter[request.url]) {
        this._responseErrorsCounter[request.url] = 0;
      }

      if (!isUserTokenExpired) {
        this._tokenStorage.main().create(userJWT);
      } else if (!isClientTokenExpired) {
        this._tokenStorage.main().create(clientJWT);
      }

      if (!refreshJWT && !mainJWT || isClientTokenExpired || isUserLoginRequest) {
        return this._updateClientToken(request, next);
      } else if (isRefreshTokenExpired) {
        this._onRefreshTokenExpiration();
      } else if (refreshJWT && isUserTokenExpired) {
        return this._auth.user().pipe(
          tap({next: (userJWT: string) => this._tokenStorage.main().create(userJWT)}),
          switchMap(() => next.handle(request)),
          catchError((result) => {
            if (result instanceof HttpErrorResponse && result.error.type === 'EXCEEDED LIMIT TOKEN REQUEST COUNT ERROR') {
              this._onRefreshTokenExpiration();
            }
            return next.handle(request);
          })
        )
      } else {
        return next.handle(request).pipe(
          catchError((result) => {
            if (result instanceof HttpErrorResponse
              && [401, 403].includes(result.status)
              && result.error.detail !== ServerErrorText.NO_PERMISSIONS) {

              const isPasswordWasUpdated: boolean = this._isIncorrectCredentials(result)
                && userJWT && !this._jwtHelper.isTokenExpired(userJWT)
                && refreshJWT && !this._jwtHelper.isTokenExpired(refreshJWT);

              if (isPasswordWasUpdated) {
                this._onRefreshTokenExpiration();
                return throwError(result);
              } else if (this._responseErrorsCounter[request.url] <= FAILED_REQUEST_COUNT_LIMIT) {
                this._responseErrorsCounter[request.url]++;

                if (refreshJWT && !this._jwtHelper.isTokenExpired(refreshJWT)) {
                  this._auth.user()
                    .pipe(tap({next: (userToken: string) => this._tokenStorage.user().create(userToken)}))
                    .subscribe(
                      () => this._http.request(request),
                      (error: HttpErrorResponse) => {
                        if (this._isIncorrectCredentials(error)) {
                          this._onRefreshTokenExpiration();
                          return throwError(result);
                        }
                      }
                    )
                } else {
                  return this._updateClientToken(request, next);
                }
              } else {
                this._zone.run(() => this._router.navigate(['whoops/something-went-wrong']));
                return throwError(result);
              }
            } else {
              return throwError(result);
            }
          })
        )
      }
    }
  }

  private _updateClientToken(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this._auth.client().pipe(
      switchMap((clientJWT: string) => {

        // @TODO: implement logic for 'Handle case when browser time is wrong'
        this._tokenStorage.main().create(clientJWT);
        return next.handle(request).pipe(
          catchError(() => {
            this._zone.run(() => this._router.navigate(['/']));
            return throwError('Request with client token was failed');
          })
        );
      })
    );
  }

  private _onRefreshTokenExpiration(): void {
    this._tokenStorage.destroyAll();
    this._zone.run(() => this._router.navigate(['auth/login']));
  }


  private _isIncorrectCredentials({status, error}: HttpErrorResponse) {
    return [401, 403].includes(status) && [
      ServerErrorText.INVALID_SIGNATURE,
      ServerErrorText.INCORRECT_AUTHENTICATION_CREDENTIALS
    ].includes(error.detail);
  }
}
