import { TestBed } from '@angular/core/testing';

import { ErrorsPageCommonService } from './errors-page-common.service';

describe('ErrorsPageCommonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ErrorsPageCommonService = TestBed.get(ErrorsPageCommonService);
    expect(service).toBeTruthy();
  });
});
