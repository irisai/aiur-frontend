import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatButtonToggleChange } from '@angular/material/button-toggle';
import { MatHorizontalStepper, MatStepperIntl } from '@angular/material/stepper';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { finalize } from 'rxjs/operators';
import { IDocumentSubmissionResponse } from '@aiur/modules/routed/explore/models/IDocumentSubmissionResponse';
import { UrlValidator } from '@aiur/core/validators/url.validator';
import { ResearchSubmissionService } from '../../services/research-submission.service';


enum DocumentSource {
  ADD_LINK = 'ADD_LINK',
  UPLOAD_FILE = 'UPLOAD_FILE'
}

@Component({
  selector: 'aif-explore-page',
  templateUrl: './explore-page.component.html',
  styleUrls: ['./explore-page.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    ResearchSubmissionService
  ]
})
export class ExplorePageComponent implements OnInit {
  @ViewChild('fileInput', {static: false}) public fileInput: ElementRef;
  @ViewChild('stepper', {static: false}) public stepper: MatHorizontalStepper;
  public documentSource: typeof DocumentSource;
  public fileSource: File;
  public chooseSourceForm: FormGroup;
  public linkInputForm: FormGroup;
  public fileInputForm: FormGroup;
  public fileName: string;
  public isLoading: boolean;
  public isFailed: boolean;
  public document: IDocumentSubmissionResponse;

  constructor(private _formBuilder: FormBuilder,
              private _stepper: MatStepperIntl,
              private _researchSubmission: ResearchSubmissionService) {

    this.documentSource = DocumentSource;
    this.isLoading = true;
    this.isFailed = false;
    this.fileName = null;
  }

  ngOnInit(): void {
    this.chooseSourceForm = this._formBuilder.group({
      source: [this.documentSource.ADD_LINK, Validators.required],
    });

    this.linkInputForm = this._formBuilder.group({
      link: [null, Validators.compose([UrlValidator, Validators.required])]
    });

    this.fileInputForm = this._formBuilder.group({
      file: [null, Validators.required],
      copyright: [false, Validators.requiredTrue]
    });
  }

  stepperChange(selectionChange: StepperSelectionEvent): void {
    const {previouslySelectedIndex, selectedIndex} = selectionChange;
    const documentSource: DocumentSource = this.chooseSourceForm.controls.source.value;
    const stepCode = `${previouslySelectedIndex}${selectedIndex}`;

    if (stepCode === '01' || stepCode === '10') {
      if (documentSource === DocumentSource.ADD_LINK) {
        this.linkInputForm.controls.link.reset();
        this.linkInputForm.controls.link.setErrors(null);
      } else if (documentSource === DocumentSource.UPLOAD_FILE) {
        this.fileInput.nativeElement.value = '';
        this.fileInputForm.controls.copyright.setValue(false);
      }
    } else if (stepCode === '12') {
      this.isLoading = true;
      this.isFailed = false;
      let data: string | File;

      if (documentSource === DocumentSource.ADD_LINK) {
        data = this.linkInputForm.controls.link.value;
      } else if (documentSource === DocumentSource.UPLOAD_FILE) {
        data = this.fileInputForm.controls.file.value
      }

      this._researchSubmission.submitDocument(data)
        .pipe(finalize(() => this.isLoading = false))
        .subscribe((result: IDocumentSubmissionResponse) => this.document = result,
          (error) => {
            this.isFailed = true;
            console.log(error);
          }
        );
    }
  }

  documentSourceUpdated({value}: MatButtonToggleChange): void {
    this.chooseSourceForm.patchValue({source: value});
  }

  cleanDocumentLink(): void {
    this.linkInputForm.controls.link.reset();
  }

  uploadFileHandler(files: FileList): void {
    this.fileInputForm.patchValue({file: files[0]});
    this.fileName = files && files.length > 0 ? files[0].name : '';
  }

  clearFileInput(): void {
    this.fileInput.nativeElement.value = '';
    this.fileInputForm.controls.file.reset();
  }

  inputValidationStep(): void {
    const documentSource: DocumentSource = this.chooseSourceForm.controls.source.value;

    if (documentSource === DocumentSource.ADD_LINK && this.linkInputForm.valid && this.linkInputForm.value.link) {
      this.stepper.next();
    } else if (documentSource === DocumentSource.UPLOAD_FILE && this.fileInputForm.valid) {
      this.stepper.next();
    }
  }

  inputStep(): void {
    this.stepper.previous();
    setTimeout(() => this.document = void (0), 100);
  }
}
