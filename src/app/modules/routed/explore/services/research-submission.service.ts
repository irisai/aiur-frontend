import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IDocumentSubmissionResponse } from '../models/IDocumentSubmissionResponse';
import { env } from 'environments/environment';


@Injectable()
export class ResearchSubmissionService {

  constructor(private _http: HttpClient) {
  }

  submitDocument(data: string | File): Observable<IDocumentSubmissionResponse> {
    let url;
    let body;

    if (data.constructor.name === 'File') {
      url = `${env.API_URL}/documents/file_upload/`;
      body = new FormData();
      body.append('file', data, (data as File).name);
    } else if (data.constructor.name === 'String') {
      url = `${env.API_URL}/documents/`;
      body = {link: data};
    }

    return this._http.post<any>(url, body).pipe(map((data: any) => JSON.parse(data) as IDocumentSubmissionResponse));
  }
}
