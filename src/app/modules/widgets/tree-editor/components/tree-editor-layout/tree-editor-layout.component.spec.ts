import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeEditorLayoutComponent } from './tree-editor-layout.component';

describe('TreeEditorLayoutComponent', () => {
  let component: TreeEditorLayoutComponent;
  let fixture: ComponentFixture<TreeEditorLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreeEditorLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeEditorLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
