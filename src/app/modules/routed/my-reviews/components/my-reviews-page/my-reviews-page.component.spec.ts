import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyReviewsPageComponent } from './my-reviews-page.component';

describe('MyReviewsPageComponent', () => {
  let component: MyReviewsPageComponent;
  let fixture: ComponentFixture<MyReviewsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyReviewsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyReviewsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
