import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TreeEditorActions } from '@aiur/modules/widgets/tree-editor';


@Component({
  selector: 'aif-tree-view-layout',
  templateUrl: './tree-view-layout.component.html',
  styleUrls: ['./tree-view-layout.component.scss']
})
export class TreeViewLayoutComponent implements OnInit {
  public documentId: string;
  public buttonActions: typeof TreeEditorActions;

  constructor(private _route: ActivatedRoute) {
    this.buttonActions = TreeEditorActions;
  }

  ngOnInit(): void {
    this.documentId = this._route.snapshot.paramMap.get('id');
  }
}
