import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FuseSharedModule } from '@fuse/shared.module';
import { DndTreeService } from './services/dnd-tree.service';
import { TreeService } from './services/tree.service';
import { TreeEditorService } from './services/tree-editor.service';
import { TreeEditorComponent } from './components/tree-editor/tree-editor.component';
import { TreeEditorLayoutComponent } from './components/tree-editor-layout/tree-editor-layout.component';
import { DocumentDetailsComponent } from './components/document-details/document-details.component';
import { TreeEditorButtonDirective } from './directives/tree-editor-button.directive';
import { TreeSuccessfulUpdatePopupComponent } from './components/tree-successful-update-popup/tree-successful-update-popup.component';


@NgModule({
  providers: [
    TreeService,
    DndTreeService,
    TreeEditorService,
  ],
  imports: [
    MatIconModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    FuseSharedModule,
  ],
  declarations: [
    TreeEditorComponent,
    TreeEditorLayoutComponent,
    DocumentDetailsComponent,
    TreeEditorButtonDirective,
    TreeSuccessfulUpdatePopupComponent
  ],
  exports: [
    TreeEditorLayoutComponent,
    TreeEditorButtonDirective
  ],
  entryComponents: [
    TreeSuccessfulUpdatePopupComponent
  ]
})
export class TreeEditorModule {
}
