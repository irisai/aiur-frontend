import { Directive, AfterContentInit, ElementRef, Output, HostListener, EventEmitter } from '@angular/core';

@Directive({
  selector: '[aifOnScrollDown]'
})
export class OnScrollDownDirective implements AfterContentInit {
  @Output() private aifOnScrollDown: EventEmitter<void>;
  private nativeElement: HTMLElement;
  private visibleHeight: number;
  private readonly threshold: number;

  constructor(private element: ElementRef) {
    this.nativeElement = element.nativeElement;
    this.aifOnScrollDown = new EventEmitter<void>();
    this.threshold = 1;
  }

  ngAfterContentInit(): void {
    this.visibleHeight = this.nativeElement.offsetHeight;
  }

  @HostListener('scroll')
  private scroll(): void {
    const scrollableHeight = this.nativeElement.scrollHeight;
    const hiddenContentHeight = scrollableHeight - this.visibleHeight;

    if (hiddenContentHeight - this.nativeElement.scrollTop <= this.threshold) {
      this.aifOnScrollDown.emit();
    }
  }
}
