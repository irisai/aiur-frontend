import { NgModule } from '@angular/core';
import { MatButtonModule, MatIconModule } from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';

import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';
import { NoPermissionPageComponent } from './components/no-permission-page/no-permission-page.component';
import { SomethingWentWrongPageComponent } from './components/something-went-wrong-page/something-went-wrong-page.component';
import { ErrorsRoutingModule } from '@aiur/modules/routed/errors/errors-routing.module';
import { ErrorsPageCommonService } from '@aiur/modules/routed/errors/services/errors-page-common.service';


@NgModule({
  providers: [ErrorsPageCommonService],
  imports: [
    MatIconModule,
    MatButtonModule,
    FuseSharedModule,
    ErrorsRoutingModule
  ],
  declarations: [
    NotFoundPageComponent,
    NoPermissionPageComponent,
    SomethingWentWrongPageComponent
  ]
})
export class ErrorsModule {

}
