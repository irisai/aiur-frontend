import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { AuthService } from '@aiur/core/services/auth.service';


@Component({
  selector: 'aif-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class LoginPageComponent implements OnInit {
  public loginForm: FormGroup;

  constructor(private _formBuilder: FormBuilder,
              private _router: Router,
              private _fuseConfigService: FuseConfigService,
              private _auth: AuthService) {
    // Configure the layout
    this._fuseConfigService.config = {
      layout: {
        navbar: {hidden: true},
        toolbar: {hidden: true},
        footer: {hidden: true},
        sidepanel: {hidden: true}
      }
    };
  }

  ngOnInit(): void {
    this.loginForm = this._formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      isRememberMe: false
    });
  }

  get form(): { [key: string]: AbstractControl } {
    return this.loginForm.controls;
  }

  login(): void {
    const {email, password, isRememberMe} = this.loginForm.value;

    if (email && password) {
      this._auth.login(email, password, isRememberMe)
        .subscribe(
          () => this._onSuccessLoginHandler(),
          () => this._onErrorLoginHandler()
        );
    }
  }

  private _onSuccessLoginHandler(): void {
    this._router.navigate(['explore']);
  }

  private _onErrorLoginHandler(): void {
    this.loginForm.setErrors({authLogin: true});

    setTimeout(() => this.loginForm.setErrors(null), 2000);
  }
}
