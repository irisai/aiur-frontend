import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';
import {
  UserExpertiseInTypesList,
  UserPositionInCompanyTypesList,
  UserSeniorityTypesList,
  UserService,
  UserUpdateError
} from '@aiur/core/services/user.service';
import { PasswordValidator } from '@aiur/core/validators/password.validator';
import { IUserTypes } from '@aiur/core/models/IUser';
import { User } from '@aiur/core/models/User';


@Component({
  selector: 'aif-user-profile-page',
  templateUrl: './user-profile-page.component.html',
  styleUrls: ['./user-profile-page.component.scss'],
  animations: fuseAnimations
})
export class UserProfilePageComponent implements OnInit {
  @ViewChild('oldPasswordInput', {static: false}) public oldPasswordInputRef: ElementRef;
  public userInfoForm: FormGroup;
  public userSeniorityTypesList: Array<IUserTypes>;
  public userExpertiseInTypesList: Array<IUserTypes>;
  public userPositionInCompanyTypesList: Array<IUserTypes>;
  public editing: boolean;
  public updated: boolean;
  public errorUpdate: boolean;
  public changingPassword: boolean;

  constructor(private _formBuilder: FormBuilder,
              private _user: UserService) {

    this.userSeniorityTypesList = UserSeniorityTypesList;
    this.userExpertiseInTypesList = UserExpertiseInTypesList;
    this.userPositionInCompanyTypesList = UserPositionInCompanyTypesList;
    this.editing = false;
    this.updated = false;
    this.errorUpdate = false;
    this.changingPassword = false;
  }

  ngOnInit(): void {
    this._user.info$.subscribe((user: User) => this._initialFormFilling(user));
  }

  get form(): { [key: string]: AbstractControl } {
    return this.userInfoForm.controls;
  }

  edit(): void {
    this.editing = true;
    this.form.first_name.enable();
    this.form.position_in_company.enable();
    this.form.expertises.enable();
    this.form.old_password.enable();
    this.form.seniority.enable();
  }

  cancelEditing(): void {
    this.editing = false;
    this.changingPassword = false;

    this._user.info$.subscribe((user: User) => {
      this.form.first_name.patchValue(user.first_name);
      this.form.position_in_company.patchValue(user.position_in_company);
      this.form.expertises.patchValue(user.expertises);
      this.form.seniority.patchValue(user.seniority);
    });

    this.form.old_password.patchValue('dummyData');
    this.form.password.patchValue('');
    this.form.password_confirm.patchValue('');

    this.form.password.clearValidators();
    this.form.password_confirm.clearValidators();
    this.form.password.updateValueAndValidity();
    this.form.password_confirm.updateValueAndValidity();

    this.form.old_password.markAsPristine();
    this.form.password.markAsPristine();
    this.form.password_confirm.markAsPristine();

    this.form.first_name.disable();
    this.form.position_in_company.disable();
    this.form.expertises.disable();
    this.form.old_password.disable();
    this.form.seniority.disable();
  }

  changePassword(): void {
    this.form.password.setValidators([PasswordValidator, Validators.required]);
    this.form.password_confirm.setValidators(Validators.required);
    this.form.password.updateValueAndValidity();
    this.form.password_confirm.updateValueAndValidity();
    this.form.old_password.patchValue('');
    this.changingPassword = true;

    this.form.old_password.markAsPristine();
    this.form.password.markAsPristine();
    this.form.password_confirm.markAsPristine();
  }

  save(): void {
    const userData = this.userInfoForm.getRawValue();

    if (this.changingPassword) {
      userData.password = (userData.password).toString().trim();
      userData.old_password = (userData.old_password).toString().trim();
      userData.password_confirm = (userData.password_confirm).toString().trim();
    } else {
      delete userData.password;
      delete userData.old_password;
      delete userData.password_confirm;
    }

    this._user.updateInfoOnServer(userData)
      .subscribe(
        () => this._onSuccessUpdate(),
        (error) => this._onErrorUpdate(error)
      );
  }

  private _initialFormFilling(user: User): void {
    this.userInfoForm = this._formBuilder.group({
      first_name: [{value: user.first_name, disabled: true}, Validators.required],
      email: [{value: user.email, disabled: true}, Validators.required],
      position_in_company: [{value: user.position_in_company, disabled: true}, Validators.required],
      expertises: [{value: user.expertises, disabled: true}, Validators.required],
      old_password: [{value: 'dummyData', disabled: true}, Validators.required],
      password: [''],
      password_confirm: [''],
      seniority: [{value: user.seniority, disabled: true}, Validators.required]
    });
  }

  private _onSuccessUpdate(): void {
    this.updated = true;
    this.cancelEditing();

    this.form.password.markAsPristine();
    this.form.old_password.markAsPristine();
    this.form.password_confirm.markAsPristine();

    setTimeout(() => this.updated = false, 2000);
  }

  private _onErrorUpdate(error: UserUpdateError): void {
    switch (error) {
      case UserUpdateError.SERVER_ERROR:
        this.errorUpdate = true;
        setTimeout(() => {
          this.errorUpdate = false;
          this.cancelEditing();
        }, 5000);
        break;
      case UserUpdateError.AUTH_ERROR:
        const oldPasswordInputEl = this.oldPasswordInputRef.nativeElement;
        oldPasswordInputEl.classList.add('error-class');
        setTimeout(() => oldPasswordInputEl.classList.remove('error-class'), 2000);
    }
  }
}
