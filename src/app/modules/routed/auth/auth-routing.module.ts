import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { RegistrationPageComponent } from './components/registration-page/registration-page.component';
import { SetupPasswordPageComponent } from '@aiur/modules/routed/auth/components/setup-password-page/setup-password-page.component';
import { RecoveryPageComponent } from '@aiur/modules/routed/auth/components/recovery-page/recovery-page.component';


const routes: Routes = [
  {path: '', redirectTo: 'login'},
  {path: 'login', component: LoginPageComponent},
  {path: 'register', component: RegistrationPageComponent},
  {path: 'password/:id', component: SetupPasswordPageComponent},
  {path: 'forgot-password', component: RecoveryPageComponent},
  {path: 'reset-password', component: LoginPageComponent},
];


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AuthRoutingModule {
}
