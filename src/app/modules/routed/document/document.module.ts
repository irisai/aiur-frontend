import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { FuseSharedModule } from '@fuse/shared.module';
import { TreeEditorModule } from '@aiur/modules/widgets/tree-editor';
import { DocumentRoutingModule } from './document-routing.module';
import { TreeReviewedLayoutComponent } from './components/tree-reviewed-layout/tree-reviewed-layout.component';
import { TreeReviewLayoutComponent } from './components/tree-review-layout/tree-review-layout.component';
import { TreeViewLayoutComponent } from './components/tree-view-layout/tree-view-layout.component';
import { TreeValidationLayoutComponent } from './components/tree-validation-layout/tree-validation-layout.component';


@NgModule({
  imports: [
    MatButtonModule,
    FuseSharedModule,
    TreeEditorModule,
    DocumentRoutingModule
  ],
  declarations: [
    TreeViewLayoutComponent,
    TreeReviewLayoutComponent,
    TreeReviewedLayoutComponent,
    TreeValidationLayoutComponent
  ]
})
export class DocumentModule {
}
