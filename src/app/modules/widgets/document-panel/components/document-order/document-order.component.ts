import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DocumentsOrdering } from '@aiur/core/models/IDocument';


interface IOrderingOptions {
  title: string;
  type: string;
}


@Component({
  selector: 'aif-document-order',
  templateUrl: './document-order.component.html',
  styleUrls: ['./document-order.component.scss']
})
export class DocumentOrderComponent implements OnInit {
  @Output() public orderingUpdated: EventEmitter<string>;
  public orderingOptions: Array<IOrderingOptions>;
  public selected: IOrderingOptions;

  constructor() {
    this.orderingUpdated = new EventEmitter<string>();

    this.orderingOptions = [
      {title: 'newest', type: DocumentsOrdering.NEWEST},
      {title: 'oldest', type: DocumentsOrdering.OLDEST}
    ];
  }

  ngOnInit(): void {
    this.selected = this.orderingOptions[0];
  }

  orderBy(option: IOrderingOptions): void {
    if (option.type === this.selected.type) {
      return;
    }
    this.selected = option;
    this.orderingUpdated.emit(option.type);
  }
}
