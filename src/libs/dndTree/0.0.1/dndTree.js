/* eslint-disable no-undef */
/*
 Copyright (c) 2013-2016, Rob Schmuecker
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.

 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.

 * The name Rob Schmuecker may not be used to endorse or promote products
 derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 'AS IS'
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL MICHAEL BOSTOCK BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.*/

/**
 * @typedef     {object} AlternativeNameData
 * @property    {number} nodeId
 * @property    {string} name
 */

/**
 * @typedef     {object} MergeData
 * @property    {number} acceptorNodeId
 * @property    {number} childNodeId
 */

/**
 * @typedef     {object} DragAndDropMetaData
 * @property    {number} draggingNodeId
 * @property    {number} selectedNodeId
 */

/**
 * @typedef     {object}  DndTreeOptions
 * @property    {boolean} editable
 */

(function (root, factory) {
  'use strict';

  if (typeof define === 'function' && typeof define.amd === 'object') {
    define([], function () {
      return factory(root);
    });
  } else {
    root.dndTree = factory(root);
  }

})(this, function () {
  'use strict';

  const dndTree = (function () {
    'use strict';

    let d3;
    let dragListener;
    let node;
    let scale;
    let x;
    let y;
    let translateCoords;
    let translateX;
    let translateY;
    let scaleX;
    let scaleY;
    let links;
    let nodePaths;
    let nodesExit;
    let parentLink;
    let dragStarted;
    let domNode;
    let relCoords;
    let panTimer;
    let nodes;
    let updateNodePropertyTimeout;


    function DndTree() {
      this.treeData = {};
      this.parentElement = '';
    }

    DndTree.prototype.init = init;

    DndTree.prototype.setTreeData = function (data) {
      this.treeData = data;
      return this;
    };

    DndTree.prototype.getTreeData = function () {
      return this.treeData;
    };

    DndTree.prototype.setParentEl = function (element) {
      this.parentElement = element;
      return this;
    };

    DndTree.prototype.getParentEl = function () {
      return this.parentElement;
    };

    DndTree.prototype.event = new EventEmitter();

    function EventEmitter() {
      this._events = Object.create(null);
    }

    EventEmitter.prototype.subscribe = function (eventName, callback) {
      let self = this;
      if (!self._events[eventName]) {
        self._events[eventName] = [];
      }

      self._events[eventName].push(callback);

      return function () {
        if (self._events[eventName]) {
          self._events[eventName] = self._events[eventName].filter(function (eventFn) {
            return eventFn !== callback;
          })
        }
      }
    };

    EventEmitter.prototype.unsubscribeAll = function () {
      this._events = Object.create(null);
    };

    EventEmitter.prototype.emit = function (eventName, data) {
      let event = this._events[eventName];

      if (event) {
        event.forEach(function (callback) {
          callback.call(null, data);
        });
      }
    };

    let dndTreeObj = new DndTree();

    /**
     * @param d3js
     * @param {DndTreeOptions} options
     * @returns {function(...[*]=)}
     */
    function init(d3js, options) {
      let isTreeEditable = (options && options.editable) || false;
      d3 = d3js;

      let treeData = this.getTreeData();

      // Calculate total nodes, max label length
      let totalNodes = 0;
      let maxLabelLength = 0;
      // variables for drag/drop
      let selectedNode = null;
      let draggingNode = null;
      // panning variables
      let panSpeed = 200;
      let panBoundary = 20; // Within 20px from edges will pan when dragging.
      // Misc. variables
      let i = 0;
      let duration = 750;
      let root;

      window.addEventListener('resize', onWindowResize);

      //Events
      dndTreeObj.event.subscribe('HE:getData', onGetData);
      dndTreeObj.event.subscribe('HE:collapseAll', onCollapseAll);
      dndTreeObj.event.subscribe('HE:expandAll', onExpandAll);
      dndTreeObj.event.subscribe('HE:expand', onExpand);
      dndTreeObj.event.subscribe('HE:collapse', onCollapse);
      dndTreeObj.event.subscribe('HE:remove', onRemove);
      dndTreeObj.event.subscribe('HE:dndTree:dragAndDrop', onDndTreeDragAndDrop);
      dndTreeObj.event.subscribe('HE:sidebar:dragAndDrop', onSidebarDragAndDrop);
      dndTreeObj.event.subscribe('HE:alternativeName', onAlternativeName);
      dndTreeObj.event.subscribe('HE:merge', onMerge);

      function onGetData() {
        dndTreeObj.event.emit('HE:data', root);
      }

      function onCollapseAll() {
        root.children.forEach(collapse);
        update(root);
        centerNode(root);
      }

      function onExpandAll() {
        root.children.forEach(collapse);
        root.children.forEach(expand);
        update(root);
        centerNode(root);
      }

      /**
       * @param {number} nodeId
       */
      function onExpand(nodeId) {
        expandNode(nodeId);
      }

      /**
       * @param {number} nodeId
       */
      function onCollapse(nodeId) {
        collapseNode(nodeId);
      }

      /**
       * @param {number} nodeId
       */
      function onRemove(nodeId) {
        function remove(node, index, array) {
          let children = node._children || node.children || [];
          if (node.id === nodeId) {
            array.splice(index, 1);
          }
          children.forEach(remove);
        }

        remove(root);
        establishMaxLabelLength();
        update(root);
      }

      /**
       * @param {DragAndDropMetaData} data
       */
      function onDndTreeDragAndDrop(data) {
        getNodeById(data.draggingNodeId, function (tmpDraggingNode) {
          draggingNode = tmpDraggingNode;

          getNodeById(data.selectedNodeId, function (selectedNode) {
            injectNode(selectedNode, tmpDraggingNode);

            expandNode(selectedNode.id);
            sortTree();
            endDrag();
          });
        });

        function injectNode(selectedNode, draggingNode) {
          let index = draggingNode.parent.children.indexOf(draggingNode);
          if (index > -1) {
            draggingNode.parent.children.splice(index, 1);
          }

          if (typeof selectedNode.children !== 'undefined' || typeof selectedNode._children !== 'undefined') {
            if (typeof selectedNode.children !== 'undefined') {
              selectedNode.children.push(draggingNode);
            } else {
              if (!selectedNode._children) {
                selectedNode._children = [];
              }
              selectedNode._children.push(draggingNode);
            }
          } else {
            selectedNode.children = [];
            selectedNode.children.push(draggingNode);
          }
        }
      }

      function onSidebarDragAndDrop(data) {
        onDndTreeDragAndDrop(data);
        update(root);
      }

      /**
       * @param {AlternativeNameData} data
       */
      function onAlternativeName(data) {
        function updateName(node) {
          let children = node._children || node.children || [];
          if (node.id === data.nodeId) {
            node.name = data.name;
          }
          children.forEach(updateName);
        }

        updateName(root);
        establishMaxLabelLength();
        update(root);
      }

      /**
       * @param {MergeData} data
       */
      function onMerge(data) {

        function merge(node) {
          let children = node._children || node.children || [];

          if (node.id === data.acceptorNodeId) {
            let childrenIndex = void (0);
            let childNode = void (0);
            let isChildrensClosed = Boolean(!node.children);

            if (isChildrensClosed) {
              node.children = node._children;
              node._children = null;
            }

            node.children.forEach(function (item, index) {
              if (item.id === data.childNodeId) {
                childrenIndex = index;
                childNode = item;
              }
            });

            let childrenOfChildren = node.children[childrenIndex].children || node.children[childrenIndex]._children;
            node.children.splice(childrenIndex, 1);

            [].push.apply(node.children, childrenOfChildren);
            node.name = node.name + ' ' + childNode.name;

            if (isChildrensClosed) {
              node._children = node.children;
              node.children = null;
            }
          }

          children.forEach(merge);
        }

        merge(root);
        establishMaxLabelLength();
        update(root);
      }

      /**
       * @param {number}      nodeId
       * @param {function}    callback
       */
      function getNodeById(nodeId, callback) {
        function getNode(node) {
          let children = node._children || node.children || [];
          if (node.id === nodeId) {
            callback(node);
          } else {
            children.forEach(getNode);
          }
        }

        getNode(root);
      }

      /**
       * @param {number} nodeId
       */
      function expandNode(nodeId) {
        function expand(node) {
          let children = node._children || node.children || [];
          if (node.id === nodeId && node._children) {
            node.children = node._children;
            node._children = null;
            update(root);
            centerNode(node);
          }
          children.forEach(expand);
        }

        expand(root);
      }

      /**
       * @param {number} nodeId
       */
      function collapseNode(nodeId) {
        function collapse(node) {
          let children = node._children || node.children || [];
          if (node.id === nodeId && node.children) {
            node._children = node.children;
            node.children = null;
            update(root);
            centerNode(node);
          }
          children.forEach(collapse);
        }

        collapse(root);
      }


      // size of the diagram
      let viewerWidth = document.querySelector(dndTreeObj.getParentEl()).offsetWidth;
      let viewerHeight = document.querySelector(dndTreeObj.getParentEl()).offsetHeight;

      let baseSvg = d3.select(dndTreeObj.getParentEl()).append('svg');
      let tree = d3.layout.tree().size([viewerHeight, viewerWidth]);

      function onWindowResize() {
        viewerWidth = document.querySelector(dndTreeObj.getParentEl()).offsetWidth;
        viewerHeight = document.querySelector(dndTreeObj.getParentEl()).offsetHeight;

        baseSvg.attr('width', viewerWidth).attr('height', viewerHeight);
      }

      // define a d3 diagonal projection for use by the node paths later on.
      let diagonal = d3.svg.diagonal()
        .source(function (d) {
          let reserve = 0;
          if (d.source.parent) {
            reserve = 155
          }
          return {
            x: d.source.x,
            y: d.source.y + reserve
          };
        })
        .projection(function (d) {
          return [d.y, d.x];
        });

      // A recursive helper function for performing some setup by walking through all nodes

      function visit(parent, visitFn, childrenFn) {
        if (!parent) return;

        visitFn(parent);

        let children = childrenFn(parent);
        if (children) {
          let count = children.length;
          for (let i = 0; i < count; i++) {
            visit(children[i], visitFn, childrenFn);
          }
        }
      }

      // Call visit function to establish maxLabelLength
      function establishMaxLabelLength() {
        maxLabelLength = 0;
        visit(treeData, function (d) {
          totalNodes++;
          // maxLabelLength = Math.max(d.name.length, maxLabelLength);
        }, function (d) {
          return d.children && d.children.length > 0 ? d.children : null;
        });
      }

      establishMaxLabelLength();

      // sort the tree according to the node names
      function sortTree() {
        tree.sort(function (a, b) {
          return b.name.toLowerCase() < a.name.toLowerCase() ? 1 : -1;
        });
      }


      function pan(domNode, direction) {
        let speed = panSpeed;
        if (panTimer) {
          clearTimeout(panTimer);
          translateCoords = d3.transform(svgGroup.attr('transform'));
          if (direction === 'left' || direction === 'right') {
            translateX = direction === 'left' ? translateCoords.translate[0] + speed : translateCoords.translate[0] - speed;
            translateY = translateCoords.translate[1];
          } else if (direction === 'up' || direction === 'down') {
            translateX = translateCoords.translate[0];
            translateY = direction === 'up' ? translateCoords.translate[1] + speed : translateCoords.translate[1] - speed;
          }
          scaleX = translateCoords.scale[0];
          scaleY = translateCoords.scale[1];
          scale = zoomListener.scale();
          svgGroup.transition().attr('transform', 'translate(' + translateX + ',' + translateY + ')scale(' + scale + ')');
          d3.select(domNode).select('g.node').attr('transform', 'translate(' + translateX + ',' + translateY + ')');
          zoomListener.scale(zoomListener.scale());
          zoomListener.translate([translateX, translateY]);
          panTimer = setTimeout(function () {
            pan(domNode, speed, direction);
          }, 50);
        }
      }

      // Define the zoom function for the zoomable tree

      function zoom() {
        svgGroup.attr('transform', 'translate(' + d3.event.translate + ')scale(' + d3.event.scale + ')');
      }

      // define the zoomListener which calls the zoom function on the 'zoom' event constrained within the scaleExtents
      let zoomListener = d3.behavior.zoom().scaleExtent([0.1, 3]).on('zoom', zoom);

      function initiateDrag(d, domNode) {
        draggingNode = d;
        d3.select(domNode).select('.ghostCircle').attr('pointer-events', 'none');
        d3.selectAll('.ghostCircle').attr('class', 'ghostCircle show');
        d3.select(domNode).attr('class', 'node activeDrag');

        svgGroup.selectAll('g.node').sort(function (a, b) { // select the parent and sort the path's
          if (a.id !== draggingNode.id) return 1; // a is not the hovered element, send 'a' to the back
          else return -1; // a is the hovered element, bring 'a' to the front
        });

        // if nodes has children, remove the links and nodes
        if (nodes.length > 1) {
          // remove link paths
          links = tree.links(nodes);
          nodePaths = svgGroup.selectAll('path.link')
            .data(links, function (d) {
              return d.target.id;
            }).remove();
          // remove child nodes
          nodesExit = svgGroup.selectAll('g.node')
            .data(nodes, function (d) {
              return d.id;
            }).filter(function (d, i) {
              return d.id !== draggingNode.id;

            }).remove();
        }

        // remove parent link
        parentLink = tree.links(tree.nodes(draggingNode.parent));
        svgGroup.selectAll('path.link').filter(function (d, i) {
          return d.target.id === draggingNode.id;

        }).remove();

        dragStarted = null;
      }

      // define the baseSvg, attaching a class for styling and the zoomListener
      baseSvg.attr('width', viewerWidth)
        .attr('height', viewerHeight)
        .attr('class', 'overlay')
        .call(zoomListener);


      // Define the drag listeners for drag/drop behaviour of nodes.
      dragListener = d3.behavior.drag()
        .on('dragstart', function (d) {
          if (d === root) {
            return;
          }
          dragStarted = true;
          nodes = tree.nodes(d);
          d3.event.sourceEvent.stopPropagation();
          // it's important that we suppress the mouseover event on the node being dragged. Otherwise it will absorb the mouseover event and the underlying node will not detect it d3.select(this).attr('pointer-events', 'none');
        })
        .on('drag', function (d) {
          if (d === root) {
            return;
          }
          if (dragStarted) {
            domNode = this;
            initiateDrag(d, domNode);
          }
          let svgElement = document.getElementsByTagName('svg');

          // getInstance coords of mouseEvent relative to svg container to allow for panning
          relCoords = d3.mouse(svgElement[0]);
          if (relCoords[0] < panBoundary) {
            panTimer = true;
            pan(this, 'left');
          } else if (relCoords[0] > (svgElement.offsetWidth - panBoundary)) {

            panTimer = true;
            pan(this, 'right');
          } else if (relCoords[1] < panBoundary) {
            panTimer = true;
            pan(this, 'up');
          } else if (relCoords[1] > (svgElement.offsetHeight - panBoundary)) {
            panTimer = true;
            pan(this, 'down');
          } else {
            try {
              clearTimeout(panTimer);
            } catch (e) {

            }
          }

          d.x0 += d3.event.dy;
          d.y0 += d3.event.dx;
          let node = d3.select(this);
          node.attr('transform', 'translate(' + d.y0 + ',' + d.x0 + ')');
          updateTempConnector();
        }).on('dragend', function (d) {
          if (d === root) {
            return;
          }
          domNode = this;

          if (selectedNode) {
            dndTreeObj.event.emit('HE:dndTree:dragAndDrop', {
              draggingNodeId: draggingNode.id,
              selectedNodeId: selectedNode.id
            });
          } else {
            endDrag();
          }
        });

      function endDrag() {
        selectedNode = null;
        d3.selectAll('.ghostCircle').attr('class', 'ghostCircle');
        d3.select(domNode).attr('class', 'node');
        // now restore the mouseover event or we won't be able to drag a 2nd time
        d3.select(domNode).select('.ghostCircle').attr('pointer-events', '');
        updateTempConnector();
        if (draggingNode !== null) {
          update(root);
          centerNode(draggingNode);
          draggingNode = null;
        }
      }

      // Helper functions for collapsing and expanding nodes.

      function collapse(d) {
        if (d.children) {
          d._children = d.children;
          d._children.forEach(collapse);
          d.children = null;
        }
      }

      function expand(d) {
        if (d._children) {
          d.children = d._children;
          d.children.forEach(expand);
          d._children = null;
        }
      }

      let overCircle = function (d) {
        selectedNode = d;
        updateTempConnector();
      };
      let outCircle = function (d) {
        selectedNode = null;
        updateTempConnector();
      };

      // Function to update the temporary connector indicating dragging affiliation
      let updateTempConnector = function () {
        let data = [];
        if (draggingNode !== null && selectedNode !== null) {
          // have to flip the source coordinates since we did this for the existing connectors on the original tree
          //noinspection JSSuspiciousNameCombination
          data = [{
            source: {
              x: selectedNode.y0,
              y: selectedNode.x0
            },
            target: {
              x: draggingNode.y0,
              y: draggingNode.x0
            }
          }];
        }
        let link = svgGroup.selectAll('.templink').data(data);

        link.enter().append('path')
          .attr('class', 'templink')
          .attr('d', d3.svg.diagonal())
          .attr('pointer-events', 'none');

        link.attr('d', d3.svg.diagonal());

        link.exit().remove();
      };

      // Function to center node when clicked/dropped so node doesn't getInstance lost when collapsing/moving with large amount of children.
      function centerNode(source) {
        scale = zoomListener.scale();
        x = -source.y0;
        y = -source.x0;
        x = x * scale + viewerWidth / 2;
        y = y * scale + viewerHeight / 2;

        d3.select('g')
          .transition()
          .duration(duration)
          .attr('transform', 'translate(' + x + ',' + y + ')scale(' + scale + ')');

        zoomListener.scale(scale);
        zoomListener.translate([x, y]);
      }

      // Toggle children on click./
      function clickOnNodeRect(node) {
        if (d3.event.defaultPrevented || !node.parent) return; // click suppressed
        dndTreeObj.event.emit((node._children ? 'HE:expand' : 'HE:collapse'), node.id);
      }

      function update(source) {
        // Compute the new height, function counts total children of root node and sets tree height accordingly.
        // This prevents the layout looking squashed when new nodes are made visible or looking sparse when nodes are removed
        // This makes the layout more consistent.
        let levelWidth = [1];
        let childCount = function (level, n) {
          if (n.children && n.children.length > 0) {
            if (levelWidth.length <= level + 1) levelWidth.push(0);

            levelWidth[level + 1] += n.children.length;
            n.children.forEach(function (d) {
              childCount(level + 1, d);
            });
          }
        };
        childCount(0, root);
        //line height for one point
        let newHeight = d3.max(levelWidth) * 70; // line height
        tree = tree.size([newHeight, viewerWidth]);

        // Compute the new tree layout.
        let nodes = tree.nodes(root).reverse(),
          links = tree.links(nodes);

        // Set widths between levels based on maxLabelLength.
        nodes.forEach(function (d) {
          maxLabelLength = maxLabelLength < 12 ? 12 : maxLabelLength;
          d.y = (d.depth * (maxLabelLength * 20)); //maxLabelLength * 10px
          // alternatively to keep a fixed scale one can set a fixed depth per level
          // Normalize for fixed-depth by commenting out below line
          // d.y = (d.depth * 500); //500px per level.
        });

        // Update the nodes…
        node = svgGroup.selectAll('g.node')
          .data(nodes, function (d) {
            return d.id || (d.id = ++i);
          });

        // Enter any new nodes at the parent's previous position.
        let nodeEnter = node
          .enter()
          .append('g')
          .call((function () {
            // Add "Drag and drop" listener just in case if DndTree is editable
            return isTreeEditable ? dragListener : function () {
            }
          }()))
          .attr('id', function (node) {
            return 'node-' + node.id
          })
          .attr('class', 'node')
          .attr('transform', function (d) {
            return 'translate(' + source.y0 + ',' + source.x0 + ')';
          });

        let rootNodeEnter = nodeEnter
          .filter(function (d) {
            return !d.parent;
          });

        let childrenNodeEnter = nodeEnter
          .filter(function (d) {
            return d.parent;
          });

        rootNodeEnter
          .append('image')
          .attr('xlink:href', 'libs/dndTree/0.0.1/images/hierarchy-tree-root.svg')
          .attr('x', '-20px')
          .attr('y', '-20px')
          .attr('width', '42px')
          .attr('height', '42px');

        rootNodeEnter
          .append("foreignObject")
          .attr('x', '-265px')
          .attr('y', '-48px')
          .attr('width', 240)
          .attr('height', 500)
          .append('xhtml:div')
          .attr('class', 'rootNode')
          .html(function (d) {
            return '<div class="container">' + d.name + '</div>'
          });

        // Add "Drag and drop" icon and "Remove node" icon just in case if DndTree is editable
        if (isTreeEditable) {
          childrenNodeEnter
            .append('image')
            .attr('class', 'closeIcon')
            .attr('xlink:href', 'libs/dndTree/0.0.1/images/remove-node-icon.svg')
            .attr('x', '160px')
            .attr('y', '-16px')
            .attr('width', '10px')
            .attr('height', '10px')
            .on('mouseover', function () {
              d3.select(this).style('visibility', 'visible');
            });

          childrenNodeEnter
            .append('image')
            .attr('class', 'dragIcon')
            .attr('xlink:href', 'libs/dndTree/0.0.1/images/hierarchy-drag-icon.svg')
            .attr('x', 5)
            .attr('y', -8)
            .attr('width', '7px')
            .attr('height', '17px');
        }

        childrenNodeEnter
          .append('rect')
          .attr({
            'class': 'selectionBorder',
            'x': '-4px',
            'y': '-21px',
            'width': '162px',
            'height': '42px'
          })
          .style({
            'fill': '#fff',
            'stroke': '#607d8b',
            'stroke-width': 2,
            'stroke-dasharray': '2, 5',
            'stroke-linecap': 'square',
            'visibility': function (node) {
              return node.isHighlighted ? 'visible' : 'hidden';
            }
          });

        childrenNodeEnter
          .append('rect')
          .attr('class', 'nodeBg')
          .attr('x', '0px')
          .attr('y', '-17px')
          .attr('width', '155px')
          .attr('height', '34px')
          .style('fill', function (d) {
            return getNodeColor(d);
          });

        function getNodeColor(node) {
          if (node.isMouseOver) {
            switch (node.metadata.type) {
              case 'original':
                return 'rgba(158,161,169,0.5)';
              case 'new':
                return 'rgba(192,236,200,0.5)';
              case 'moved':
                return 'rgba(231,236,240,0.5)';
              case 'deleted':
                return 'rgba(238,133,131,0.5)';
            }
          } else {
            switch (node.metadata.type) {
              case 'original':
                return '#9ea1a9';
              case 'new':
                return '#c0ecc8';
              case 'moved':
                return '#e7ecf0';
              case 'deleted':
                return '#ee8583';
            }
          }
        }

        childrenNodeEnter
          .append('title')
          .attr('class', 'nodeTitle')
          .text(function (d) {
            return d.name;
          });

        // Label clickable area
        // hide for firs node and more width for nodes without children
        childrenNodeEnter
          .append('rect')
          .attr('x', '15px')
          .attr('y', '-17px')
          .attr('width', function (d) {
            let hasChildren = !((d._children && d._children.length === 0) || (!d._children && !d.children));
            return hasChildren ? '105px' : '140px';
          })
          .attr('height', '34px')
          .attr('class', 'labelClick')
          .style('fill', 'transparent');

        childrenNodeEnter
          .append('text')
          .attr('x', '15px')
          .attr('y', '5px')
          .attr('width', '105px')
          .attr('height', '34px')
          .attr('class', 'nodeText')
          .text(function (node) {
            return node.name;
          });

        childrenNodeEnter
          .append('rect')
          .attr('class', 'nodeRect')
          .attr('x', '123px')
          .attr('y', '-15px')
          .attr('rx', '3px')
          .attr('ry', '3px')
          .attr('width', '30px')
          .attr('height', '30px')
          .style('fill', '#fff')
          .style('visibility', function (d) {
            return d.papers ? 'hidden' : 'visible';
          });

        // phantom node to give us mouseover in a radius around it
        nodeEnter
          .append('circle')
          .attr('class', 'ghostCircle')
          .attr('r', 45)
          .attr('opacity', 0.1) // change this to zero to hide the target area
          .style('fill', 'black')
          .attr('pointer-events', 'mouseover')
          .on('mouseover', function (node) {
            overCircle(node);
          })
          .on('mouseout', function (node) {
            outCircle(node);
          });

        // Hover node
        childrenNodeEnter
          .on('mouseover', function (node) {
            nodeOnMouseOver(d3.select(this), node);
          })
          .on('mouseout', function (node) {
            nodeOnMouseOut(d3.select(this), node);
          });

        function highlightPreviousNodeState(node, isVisible) {
          let parentSearchNodeId;
          const currentNodeType = node.metadata.type;

          if (currentNodeType === 'new' || currentNodeType === 'moved') {
            switch (currentNodeType) {
              case 'new':
                parentSearchNodeId = node.metadata.previousParent;
                break;
              case 'moved':
                parentSearchNodeId = node.metadata.currentParent;
                break;
            }
            const highlightNodeId = getNodeIdByParentIdAndNodeName(parentSearchNodeId, node);

            if (highlightNodeId) {
              d3.select(`#node-${highlightNodeId} .selectionBorder`).style({
                'visibility': isVisible ? 'visible' : 'hidden'
              });
            }
          }
        }

        function getNodeIdByParentIdAndNodeName(parentId, hoveredNode) {
          const parentNode = d3.select(`#node-${parentId}`).data()[0];

          if (parentNode && parentNode.children) {
            return parentNode.children.filter(function (childNode) {
              return childNode.name === hoveredNode.name
                && ['moved', 'new'].includes(childNode.metadata.type)
                && [childNode.metadata.currentParent, childNode.metadata.previousParent]
                  .includes(hoveredNode.parent.id)
            })[0].id;
          }
        }

        // Update highlighting style to reflect on data structure update
        node.select('.selectionBorder')
          .style('visibility', function (node) {
            return node.isHighlighted ? 'visible' : 'hidden';
          });

        // Update the text to reflect whether node has children or not.
        node.select('text')
          .text(function (d) {
            return d.name;
          })
          .each(wordsShortening)
          .on('click', onLabelClick);

        node.select('rect.labelClick')
          .on('click', onLabelClick);

        function onLabelClick(node) {
          if (!node.parent) return;

          node._children && clickOnNodeRect(node);
          dndTreeObj.event.emit('HE:onLabelClick', Object.assign({}, node));
        }

        function wordsShortening(d) {
          if (!d.parent) return;

          let self = d3.select(this);
          let textWidth = self.node().getComputedTextLength();
          let text = self.text();

          let maxWidth = 102;

          if ((d._children && d._children.length === 0) || (!d._children && !d.children)) {
            maxWidth = 135;
          }

          while (textWidth > maxWidth && text.length > 0) {
            text = text.slice(0, -1);
            self.text(text + '...');
            textWidth = self.node().getComputedTextLength();
          }
        }

        // Change the rect icon depending on whether it has children and is collapsed
        node.select('rect.nodeRect')
          .style('fill', function (d) {
            return d._children ? 'url("#plus")' : 'url("#minus")';
          })
          .on('click', clickOnNodeRect);

        node.select('image.closeIcon')
          .attr('opacity', 0)
          .on('click', function (node) {
            if (nodes.length > 2) {
              dndTreeObj.event.emit('HE:remove', node.id);
            }
          });

        function nodeOnMouseOver(targetNode, node) {
          node.isMouseOver = true;
          targetNode.select('.nodeBg').style({'fill': getNodeColor(node)});
          targetNode.select('.closeIcon').attr('opacity', 1);
          targetNode.select('.dragIcon').attr('opacity', 1);

          highlightPreviousNodeState(node, true);
        }

        function nodeOnMouseOut(targetNode, node) {
          node.isMouseOver = false;
          targetNode.select('.nodeBg').style({'fill': getNodeColor(node)});
          targetNode.select('.closeIcon').attr('opacity', 0);
          targetNode.select('.dragIcon').attr('opacity', 0);

          highlightPreviousNodeState(node, false);
        }

        // Transition nodes to their new position.
        let nodeUpdate = node.transition()
          .duration(duration)
          .attr('transform', function (d) {
            return 'translate(' + d.y + ',' + d.x + ')';
          });

        nodeUpdate.select('.nodeRect')
          .style('visibility', function (d) {
            return ((d._children && d._children.length > 0) || d.children) ? 'visible' : 'hidden';
          });

        nodeUpdate.select('.nodeTitle')
          .text(function (d) {
            return d.name;
          });

        // Fade the text in
        nodeUpdate.select('text')
          .each(wordsShortening)
          .style('fill-opacity', 1);

        // Transition exiting nodes to the parent's new position.
        let nodeExit = node.exit().transition()
          .duration(duration)
          .attr('transform', function (d) {
            return 'translate(' + source.y + ',' + source.x + ')';
          })
          .remove();

        nodeExit.select('circle')
          .attr('r', 0);

        nodeExit.select('.nodeTitle')
          .text(function (d) {
            return d.name;
          });

        nodeExit.select('text')
          .style('fill-opacity', 0);

        // Update the links…
        let link = svgGroup.selectAll('path.link')
          .data(links, function (d) {
            return d.target.id;
          });

        // Enter any new links at the parent's previous position.
        link.enter().insert('path', 'g')
          .attr('class', 'link')
          .attr('d', function (d) {
            let o = {
              x: source.x0,
              y: source.y0
            };
            return diagonal({
              source: o,
              target: o
            });
          });

        // Transition links to their new position.
        link.transition()
          .duration(duration)
          .attr('d', diagonal);

        // Transition exiting nodes to the parent's new position.
        link.exit().transition()
          .duration(duration)
          .attr('d', function (d) {
            let o = {
              x: source.x,
              y: source.y
            };
            return diagonal({
              source: o,
              target: {
                x: source.x,
                y: source.y + 155
              }
            });
          }).remove();

        // Stash the old positions for transition.
        nodes.forEach(function (d) {
          d.x0 = d.x;
          d.y0 = d.y;
        });
      }

      // Append a group which holds all nodes and which the zoom Listener can act upon.
      let svgGroup = baseSvg.append('g');
      svgGroup.append('pattern')
        .attr('id', 'plus')
        .attr('class', 'svg-image')
        .attr('x', '0')
        .attr('y', '0')
        .attr('height', '1')
        .attr('width', '1')
        .append('image')
        .attr('x', '0px')
        .attr('y', '0px')
        .attr('height', '30px')
        .attr('width', '30px')
        .attr('xlink:href', 'libs/dndTree/0.0.1/images/plus-rect-icon.svg');

      svgGroup.append('pattern')
        .attr('id', 'minus')
        .attr('class', 'svg-image')
        .attr('x', '0')
        .attr('y', '0')
        .attr('height', '1')
        .attr('width', '1')
        .append('image')
        .attr('x', '0px')
        .attr('y', '0px')
        .attr('height', '30px')
        .attr('width', '30px')
        .attr('xlink:href', 'libs/dndTree/0.0.1/images/minus-rect-icon.svg');

      // Define the root
      root = treeData;
      root.x0 = viewerHeight / 2;
      root.y0 = 0;
      root.children.forEach(collapse);

      // Layout the tree initially and center on the root node.
      update(root);
      centerNode(root);

      return function () {
        window.removeEventListener('resize', onWindowResize)
      }
    }

    return dndTreeObj;

  }());

  return window.dndTree = dndTree;
});
