import { IUser } from '@aiur/core/models/IUser';


export class User implements IUser {
  user_id: number;
  username: string;
  exp: Date;
  email: string;
  id: number;
  first_name: string;
  position_in_company: string;
  expertises: Array<string>;
  seniority: string;
  type: string;
  image: string;

  constructor(public user: IUser) {
    this.user_id = user.user_id;
    this.username = user.username;
    this.exp = user.exp;
    this.email = user.email;
    this.id = user.id;
    this.first_name = user.first_name;
    this.position_in_company = user.position_in_company;
    this.expertises = user.expertises;
    this.seniority = user.seniority;
    this.type = user.type;
    this.image = user.image;
  }
}
