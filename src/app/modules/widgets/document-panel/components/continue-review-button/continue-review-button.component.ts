import { Component, Input } from '@angular/core';
import { IDocument } from '@aiur/core/models/IDocument';


@Component({
  selector: 'aif-continue-review-button',
  template: `
    <button [routerLink]="['/document', documentId, 'tree-review', 'origin']" mat-raised-button class="mat-accent mr-16">Continue review</button>
  `
})
export class ContinueReviewButtonComponent {
  @Input() public documentId: number;
}
