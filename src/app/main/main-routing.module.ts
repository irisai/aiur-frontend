import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard, UnauthorizedGuard } from '@aiur/core/guards';


const routes: Routes = [
  {
    path: 'auth',
    canActivate: [UnauthorizedGuard],
    loadChildren: () => import('../modules/routed/auth/auth.module')
      .then(module => module.AuthModule)
  },
  {path: '', redirectTo: 'explore', pathMatch: 'full'},
  {
    path: 'explore',
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    loadChildren: () => import('../modules/routed/explore/explore.module')
      .then(module => module.ExploreModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('../modules/routed/user-profile/user-profile.module')
      .then(module => module.UserProfileModule)
  },
  {
    path: 'my-trees',
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    loadChildren: () => import('../modules/routed/my-trees/my-trees.module')
      .then(module => module.MyTreesModule)
  },
  {
    path: 'my-reviews',
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    loadChildren: () => import('../modules/routed/my-reviews/my-reviews.module')
      .then(module => module.MyReviewsModule)
  },
  {
    path: 'for-review',
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    loadChildren: () => import('../modules/routed/for-review/for-review.module')
      .then(module => module.ForReviewModule)
  },
  {
    path: 'tree',
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    loadChildren: () => import('../modules/routed/tree/tree.module')
      .then(module => module.TreeModule),
  },
  {
    path: 'document/:id',
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    loadChildren: () => import('../modules/routed/document/document.module')
      .then(module => module.DocumentModule),
  },
  {
    path: 'errors',
    loadChildren: () => import('../modules/routed/errors/errors.module')
      .then(module => module.ErrorsModule)
  },
  {
    path: '**',
    redirectTo: 'auth',
    pathMatch: 'full'
  }
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      onSameUrlNavigation: 'reload',
      paramsInheritanceStrategy: 'always'
    })
  ],
  exports: [RouterModule]
})
export class MainRoutingModule {
}
