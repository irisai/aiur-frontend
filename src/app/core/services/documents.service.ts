import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { IDocument } from '@aiur/core/models/IDocument';
import { env } from '@config/environment';
import { removeEmptyReqParams } from '@aiur/core/utils/removeEmptyReqParams';


export interface IDocumentHttpReqParams {
  page?: number;
  search?: string;
  ordering?: string;
  for_review?: boolean;
  reviewed?: boolean;
}

export interface IDocumentHttpResponse {
  count: number;
  next: string | null;
  previous: string | null;
  results: Array<IDocument>;
}

@Injectable({
  providedIn: 'root'
})
export class DocumentsService {
  public forReview: boolean;
  public reviewed: boolean;
  public page: number;
  public search: string;
  public ordering: string;
  public count: number;
  public loading: boolean;
  public loading$: Observable<boolean>;
  private _loadingSubject: BehaviorSubject<boolean>;
  private _list: Array<IDocument>;
  public list$: Observable<Array<IDocument>>;
  public _listSubject: Subject<Array<IDocument>>;

  constructor(private _http: HttpClient) {
    this._loadingSubject = new BehaviorSubject<boolean>(false);
    this.loading$ = this._loadingSubject.asObservable();
    this._listSubject = new Subject<Array<IDocument>>();
    this.list$ = this._listSubject.asObservable();
    this._list = [];
  }

  init(requestParams): DocumentsService {
    this.page = void (0);
    this.count = void (0);
    this.search = void (0);
    this.ordering = void (0);
    this.forReview = void (0);
    this.reviewed = void (0);
    this._list.length = 0;
    this._listSubject.next(this._list);
    Object.assign(this, requestParams);

    return this;
  }

  load(): void {
    if (this.loading || this.count === this._list.length && this._list.length !== 0) {
      return;
    }

    this.loading = true;
    this._loadingSubject.next(true);

    const params: IDocumentHttpReqParams = {
      for_review: this.forReview,
      reviewed: this.reviewed,
      page: this.page,
      search: this.search,
      ordering: this.ordering
    };

    this.getList(params)
      .pipe(finalize(() => {
        this.loading = false;
        this._loadingSubject.next(false);
      }))
      .subscribe(({results, count}) => {
        for (const document of results) {
          this._list.push(document);
        }

        this._listSubject.next(this._list);
        this.count = count;
        this.page = this.page ? (this.page + 1) : 2;
      });
  }

  getList(httpParams: IDocumentHttpReqParams): Observable<IDocumentHttpResponse> {
    const url = `${env.API_URL}/documents/`;

    const httpOptions = {
      params: new HttpParams({
        fromObject: removeEmptyReqParams({...httpParams, ...{date: Date.now().toString()}})
      })
    };

    return this._http.get<IDocumentHttpResponse>(url, httpOptions);
  }

  getById(id: string): Observable<IDocument> {
    const url = `${env.API_URL}/documents/${id}/`;

    const options = {
      params: new HttpParams().set('date', Date.now().toString())
    };

    return this._http.get<IDocument>(url, options);
  }

  searching(query: string): void {
    this.search = query ? query : void (0);
    this._update();
  }

  orderBy(type: string): void {
    if (!type) {
      throw new Error('method called without required parameter - "order type"');
    }

    this.ordering = type;
    this._update();
  }

  private _update(): void {
    this.page = void (0);
    this._list.length = 0;
    this._listSubject.next(this._list);
    this.load();
  }

}
