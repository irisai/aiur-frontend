import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyReviewsPageComponent } from './components/my-reviews-page/my-reviews-page.component';


const routes: Routes = [
  {path: '', component: MyReviewsPageComponent}
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyReviewsRoutingModule {
}
