import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { DocumentPanelModule } from '@aiur/modules/widgets/document-panel/document-panel.module';
import { ForReviewPageComponent } from './components/for-review-page/for-review-page.component';
import { ForReviewRoutingModule } from './for-review-routing.module';
import { NoResultModule } from '@aiur/modules/widgets/no-result/no-result.module';



@NgModule({
  imports: [
    MatIconModule,
    FuseSharedModule,
    ForReviewRoutingModule,
    DocumentPanelModule,
    NoResultModule
  ],
  declarations: [
    ForReviewPageComponent
  ]
})
export class ForReviewModule { }
