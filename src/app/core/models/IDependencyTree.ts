export interface IDependencyTree {
  title: string;
  year: number;
  identifier: string;
  journal: string;
  fulltext_link: string;
  download_url: string;
  abstract: string;
  fingerprint: Array<string>;
  name: string;
  metadata: IDndTreeMetadata;
  children: Array<IDependencyTree>;
  ord_id: number;
}

export interface IDndTreeMetadata {
  type: 'original' | 'new' | 'moved' | 'deleted';
  currentParent?: number;
  previousParent?: number;
}
