import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { IDocument } from '@aiur/core/models/IDocument';
import { IDependencyTreeMetadata } from '@aiur/core/models/IDependencyTreeMetadata';
import { DependencyTreeType } from '@aiur/core/models/DependencyTreeType';
import { DependencyTreeStatus } from '@aiur/core/models/DependencyTreeStatus';


@Component({
  selector: 'aif-document-reviewers-list',
  templateUrl: './document-reviewers-list.component.html',
  styleUrls: ['./document-reviewers-list.component.scss'],
})
export class DocumentReviewersListComponent implements OnInit {
  @Input() public document: IDocument;
  @HostBinding('class.top-space') isReviewersExist: boolean;
  public treesStatus: typeof DependencyTreeStatus;
  public reviewers: Array<IDependencyTreeMetadata>;

  constructor() {
    this.treesStatus = DependencyTreeStatus;
  }

  ngOnInit(): void {
    this.reviewers = this.document.trees
      .filter(({type}: IDependencyTreeMetadata) => {
        return type === DependencyTreeType.REVIEW
      });

    this.isReviewersExist = Boolean(this.reviewers.length);
  }
}
