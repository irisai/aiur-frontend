import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { concatMap, map, shareReplay, tap } from 'rxjs/operators';
import { fuseAnimations } from '@fuse/animations';
import { User } from '@aiur/core/models/User';
import { DocumentsOrdering, IDocument } from '@aiur/core/models/IDocument';
import { DocumentsService } from '@aiur/core/services/documents.service';
import { UserService } from '@aiur/core/services/user.service';
import { IDependencyTreeMetadata } from '@aiur/core/models/IDependencyTreeMetadata';
import { DependencyTreeType } from '@aiur/core/models/DependencyTreeType';


@Component({
  selector: 'aif-my-reviews-page',
  templateUrl: './my-reviews-page.component.html',
  styleUrls: ['./my-reviews-page.component.scss'],
  animations: fuseAnimations
})
export class MyReviewsPageComponent implements OnInit {
  public documentsList$: Observable<Array<IDocument>>;
  public isLoading$: Observable<boolean>;

  constructor(private _user: UserService,
              private _documents: DocumentsService) {
  }

  ngOnInit(): void {
    this.isLoading$ = this._documents.loading$;
    this.documentsList$ = this._documents.list$.pipe(
      concatMap((documentsList: Array<IDocument>) => this._user.info$.pipe(map(({user_id}: User) => [documentsList, user_id]))),
      map(([documentsList, user_id]: [Array<IDocument>, number]) => {
        return documentsList.map((document) => {
          const {status, created_at, modified_at} = document.trees
            .filter(({type, owner_id}: IDependencyTreeMetadata) => {
              return owner_id === user_id && type === DependencyTreeType.REVIEW
            }).pop();

          return Object.assign(document, {
            status, created_at, modified_at
          });
        });
      }),
      shareReplay({refCount: true})
    );

    this._documents.init({
      reviewed: true,
      ordering: DocumentsOrdering.NEWEST
    });
    this._documents.load();
  }

  documentSearchChange(query: string): void {
    this._documents.searching(query);
  }

  documentOrderChange(orderingType: string): void {
    this._documents.orderBy(orderingType);
  }

  trackByFn(index: number): number {
    return index;
  }
}
