import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeReviewLayoutComponent } from './tree-review-layout.component';

describe('MyReviewLayoutComponent', () => {
  let component: TreeReviewLayoutComponent;
  let fixture: ComponentFixture<TreeReviewLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreeReviewLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeReviewLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
