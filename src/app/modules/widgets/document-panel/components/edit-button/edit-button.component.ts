import { Component, Input } from '@angular/core';


@Component({
  selector: 'aif-edit-button',
  template: `
    <button [routerLink]="['/document', documentId, 'tree', 'origin']" mat-raised-button class="mat-accent mr-16">Edit</button>
  `
})
export class EditButtonComponent {
  @Input() public documentId: number;
}
