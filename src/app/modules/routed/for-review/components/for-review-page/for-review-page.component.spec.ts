import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForReviewPageComponent } from './for-review-page.component';

describe('ForReviewPageComponent', () => {
  let component: ForReviewPageComponent;
  let fixture: ComponentFixture<ForReviewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForReviewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForReviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
