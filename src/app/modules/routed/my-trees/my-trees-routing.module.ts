import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyTreesPageComponent } from './components/my-trees-page/my-trees-page.component';


const routes: Routes = [
  {path: '', component: MyTreesPageComponent}
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyTreesRoutingModule {
}
