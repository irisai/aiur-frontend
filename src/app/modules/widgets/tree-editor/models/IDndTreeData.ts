import { IDependencyTree } from '@aiur/core/models/IDependencyTree';


export interface IDndTreeData extends IDependencyTree {
  id: number;
  depth: number;
  isMouseOver: boolean;
  isHighlighted: boolean;
  children: Array<IDndTreeData>;
  _children: Array<IDndTreeData>;
}
