import { Provider } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { HttpErrorResponseInterceptor } from './http-error-response.interceptor';
import { WithCredentialsInterceptor } from './with-credentials.interceptor';
import { FakeBackendInterceptor } from './fake-backend.interceptor';
import { AuthInterceptor } from './auth.interceptor';


export const HttpInterceptorProviders: Array<Provider> = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: HttpErrorResponseInterceptor,
    multi: true
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: WithCredentialsInterceptor,
    multi: true
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }
];
