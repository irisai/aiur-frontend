export function removeEmptyReqParams(params): { [key: string]: string } {
  const httpParams = Object.create(null);
  Object.keys(params).forEach((key) => {
    if (params[key] || params[key] === 0) {
      httpParams[key] = params[key].toString();
    }
  });
  return httpParams;
}
