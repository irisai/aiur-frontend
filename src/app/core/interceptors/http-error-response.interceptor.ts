import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';



@Injectable()
export class HttpErrorResponseInterceptor implements HttpInterceptor {

  constructor(private _zone: NgZone,
              private _router: Router) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((error) => {

        if (error instanceof HttpErrorResponse) {
          if (this._isNotFound(error) && !this._isUrlException(error)) {
            this._zone.run(() => this._router.navigate(['whoops/not-found']));
          } else if (this._isForbidden(error)) {
            this._zone.run(() => this._router.navigate(['whoops/no-permission']));
          } else if (this._isServerError(error)) {
            this._zone.run(() => this._router.navigate(['whoops/something-went-wrong']));
          }
        }
        return throwError(error);
      })
    );
  }

  private _isNotFound({status}: HttpErrorResponse): boolean {
    return 404 === status;
  }

  private _isServerError({status}: HttpErrorResponse): boolean {
    return (/^5\d{2}/gi).test(status.toString());
  }

  private _isForbidden({status, error: {detail}}: HttpErrorResponse): boolean {
    return 403 === status && detail === 'You do not have permission to perform this action.';
  }

  private _isUrlException({url}: HttpErrorResponse): boolean {
    return (/users\/reset-password/gi).test(url);
  }
}
