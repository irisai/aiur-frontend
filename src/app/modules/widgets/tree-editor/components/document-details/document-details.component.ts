import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IDocument } from '@aiur/core/models/IDocument';
import { fuseAnimations } from '@fuse/animations';


@Component({
  selector: 'aif-document-details',
  templateUrl: './document-details.component.html',
  styleUrls: ['./document-details.component.scss'],
  animations: fuseAnimations
})
export class DocumentDetailsComponent implements OnInit {
  @Input() public document: IDocument;
  @Output() public closed: EventEmitter<void>;
  public isShowMore: boolean;

  constructor() {
    this.isShowMore = false;
    this.closed = new EventEmitter<void>();
  }

  ngOnInit(): void {
  }

  showMoreToggle(): void {
    this.isShowMore = !this.isShowMore;
  }

  close(): void {
    this.closed.emit();
  }

  goToFullTextLink(fullTextLink: string): void {
    window.open(fullTextLink, '_blank');
  }
}
