import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatIconModule, MatInputModule,
  MatRadioModule,
  MatSelectModule
} from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { UserProfileRoutingModule } from './user-profile-routing.module';
import { UserProfilePageComponent } from './components/user-profile-page/user-profile-page.component';



@NgModule({
  imports: [
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    MatCheckboxModule,
    FuseSharedModule,
    UserProfileRoutingModule
  ],
  declarations: [UserProfilePageComponent]
})
export class UserProfileModule { }
