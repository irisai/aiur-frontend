import { Component, OnInit } from '@angular/core';
import { TreeEditorActions } from '@aiur/modules/widgets/tree-editor';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'aif-tree-review-layout',
  templateUrl: './tree-review-layout.component.html',
  styleUrls: ['./tree-review-layout.component.scss']
})
export class TreeReviewLayoutComponent implements OnInit {
  public documentId: string;
  public buttonActions: typeof TreeEditorActions;

  constructor(private _route: ActivatedRoute,
              private _router: Router) {

    this.buttonActions = TreeEditorActions;
  }

  ngOnInit(): void {
    this.documentId = this._route.snapshot.paramMap.get('id');
  }

  isActiveType(type: string): boolean {
    return this._router.url.endsWith(type);
  }
}
