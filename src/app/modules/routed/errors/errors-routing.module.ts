import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';
import { NoPermissionPageComponent } from './components/no-permission-page/no-permission-page.component';
import { SomethingWentWrongPageComponent } from './components/something-went-wrong-page/something-went-wrong-page.component';


const routes: Routes = [
  {path: '', redirectTo: 'something-went-wrong'},
  {path: 'not-found', component: NotFoundPageComponent},
  {path: 'no-permission', component: NoPermissionPageComponent},
  {path: 'something-went-wrong', component: SomethingWentWrongPageComponent}
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErrorsRoutingModule {
}
