import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IDependencyTree } from '@aiur/core/models/IDependencyTree';
import { IDependencyTreeMetadata } from '@aiur/core/models/IDependencyTreeMetadata';
import { ITreeResponse } from '../models/ITreeResponse';
import { env } from '@config/environment';


export type SubmitDependencyTreeType =
  Pick<IDependencyTreeMetadata, ('type' | 'document_id' | 'status' | 'owner_id')>
  & { dependency_tree: IDependencyTree };


@Injectable()
export class TreeService {
  constructor(private _http: HttpClient) {
  }

  getById(id: number): Observable<ITreeResponse> {
    const url = `${env.API_URL}/trees/${id}/`;

    const options = {
      params: new HttpParams().set('date', Date.now().toString())
    };

    return this._http.get<ITreeResponse>(url, options);
  }

  createReview(data: SubmitDependencyTreeType): Observable<IDependencyTreeMetadata> {
    const url = `${env.API_URL}/trees/`;
    const body: SubmitDependencyTreeType = {...data};

    return this._http.post<IDependencyTreeMetadata>(url, body);
  }

  updateReview(id: number, data: SubmitDependencyTreeType): Observable<IDependencyTreeMetadata> {
    const url = `${env.API_URL}/trees/${id}/`;
    const body: SubmitDependencyTreeType = {...data};

    return this._http.put<IDependencyTreeMetadata>(url, body);
  }

  compareTree(id: number): Observable<IDependencyTree> {
    const url = `${env.API_URL}/trees/${id}/compare/`;

    return this._http.get<IDependencyTree>(url);
  }
}
