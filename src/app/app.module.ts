import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TranslateModule } from '@ngx-translate/core';

import { FuseModule } from '@fuse/fuse.module';
import { fuseConfig } from '@aiur/fuse-config';

import { CoreModule } from '@aiur/core/core.module';
import { MainModule } from '@aiur/main/main.module';
import { AppComponent } from '@aiur/app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,

    TranslateModule.forRoot(),

    // Fuse modules
    FuseModule.forRoot(fuseConfig),

    // App core module
    CoreModule,

    // App modules
    MainModule
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}
