import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TreePageComponent } from './components/tree-page/tree-page.component';


const routes: Routes = [
  {path: ':id', component: TreePageComponent}
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TreeRoutingModule {
}
