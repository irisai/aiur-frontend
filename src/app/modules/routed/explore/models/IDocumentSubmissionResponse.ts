import { IDependencyTree } from '@aiur/core/models/IDependencyTree';


export interface IDocumentSubmissionResponse extends IDependencyTree {
  tree_id: number;
  document_id: number;
}
