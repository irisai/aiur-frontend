import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetupPasswordPageComponent } from './setup-password-page.component';

describe('SetupPasswordPageComponent', () => {
  let component: SetupPasswordPageComponent;
  let fixture: ComponentFixture<SetupPasswordPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetupPasswordPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetupPasswordPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
