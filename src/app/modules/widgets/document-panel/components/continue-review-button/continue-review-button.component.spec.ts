import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContinueReviewButtonComponent } from './continue-review-button.component';

describe('ContinueReviewButtonComponent', () => {
  let component: ContinueReviewButtonComponent;
  let fixture: ComponentFixture<ContinueReviewButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContinueReviewButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContinueReviewButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
