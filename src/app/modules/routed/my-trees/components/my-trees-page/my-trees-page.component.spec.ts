import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyTreesPageComponent } from './my-trees-page.component';

describe('MyTreesPageComponent', () => {
  let component: MyTreesPageComponent;
  let fixture: ComponentFixture<MyTreesPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyTreesPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyTreesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
