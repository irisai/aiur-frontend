import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { NoResultComponent } from './no-result/no-result.component';


@NgModule({
  declarations: [NoResultComponent],
  imports: [FuseSharedModule],
  exports: [NoResultComponent]
})
export class NoResultModule {
}
