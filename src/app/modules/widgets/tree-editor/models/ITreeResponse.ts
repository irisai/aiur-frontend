import { IDependencyTree } from '@aiur/core/models/IDependencyTree';


export interface ITreeResponse {
  id: number;
  type: 'submission',
  document_id: number;
  status: 'submitted';
  owner_id: number;
  dependency_tree: IDependencyTree
}
