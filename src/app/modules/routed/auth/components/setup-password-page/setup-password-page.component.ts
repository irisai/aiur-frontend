import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { fuseAnimations } from '@fuse/animations';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { noop } from 'rxjs';
import { FuseConfigService } from '@fuse/services/config.service';
import { PasswordValidator } from '@aiur/core/validators/password.validator';
import { UserService } from '@aiur/core/services/user.service';


@Component({
  selector: 'aif-setup-password-page',
  templateUrl: './setup-password-page.component.html',
  styleUrls: ['./setup-password-page.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations
})
export class SetupPasswordPageComponent implements OnInit {
  setupPasswordForm: FormGroup;
  jwtHelper: JwtHelperService;
  isLinkExpired: boolean;
  success: boolean;
  userId: number;
  expirationTime: Date;
  currentTime: Date;
  recoveryParams: string;

  constructor(private _fuseConfigService: FuseConfigService,
              private _formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private user: UserService) {
    this.jwtHelper = new JwtHelperService();
    this.isLinkExpired = false;
    this.success = false;
    this.userId = +this.route.snapshot.paramMap.get('id');
    // Configure the layout
    this._fuseConfigService.config = {
      layout: {
        navbar   : {
          hidden: true
        },
        toolbar  : {
          hidden: true
        },
        footer   : {
          hidden: true
        },
        sidepanel: {
          hidden: true
        }
      }
    };
  }

  ngOnInit(): void {
    this.setupPasswordForm = this._formBuilder.group({
      password: ['', [PasswordValidator, Validators.required]],
      password_confirm: ['', [Validators.required]]
    });
    try {
      const token = this.route.snapshot.queryParams.token;
      if (token && this.jwtHelper.isTokenExpired(token)) {
        this.expirationTime = this.jwtHelper.getTokenExpirationDate(token);
        this.currentTime = new Date();
        this.recoveryParams = token;
        this.isLinkExpired = true;
      }
    } catch (error) {
      this.router.navigate(['auth/recovery']).then(noop);
      // @TODO: Add LoggingService logError function
      console.log(error);
    }
  }

  get form(): { [key: string]: AbstractControl } {
    return this.setupPasswordForm.controls;
  }

  submit(): void {
    const passwordData = {
      password: this.form.password.value,
      password_confirm: this.form.password_confirm.value
    };

    this.user.setUpPassword(this.userId, this.route.snapshot.queryParams.token, passwordData).subscribe(
      () => this._successSubmitHandle(),
      (error) => this._errorSubmitHandle(error)
    );
  }

  private _successSubmitHandle(): void {
    this.success = true;
    setTimeout(() => {
      this.router.navigate(['auth/login']).then(noop);
    }, 2500);
  }

  private _errorSubmitHandle(error): void {
    if (this._checkIsLinkExpired(error)) {
      this.isLinkExpired = true;
    } else {
      console.log(error);
    }
  }

  private _checkIsLinkExpired(result): boolean {
    return 403 === result.status && 'Incorrect authentication credentials.' === result.detail;
  }

}
