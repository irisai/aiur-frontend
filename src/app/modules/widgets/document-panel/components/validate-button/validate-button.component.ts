import { Component, Input } from '@angular/core';


@Component({
  selector: 'aif-validate-button',
  template: `
    <button [routerLink]="['/document', documentId, 'tree-validation', 'origin']" mat-raised-button class="mat-accent mr-16">Validate</button>
  `
})
export class ValidateButtonComponent {
  @Input() public documentId: number;
}
