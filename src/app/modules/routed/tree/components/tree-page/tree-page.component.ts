import { Component, OnInit } from '@angular/core';
import { TreeEditorActions } from '@aiur/modules/widgets/tree-editor';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'aif-tree-page',
  templateUrl: './tree-page.component.html',
  styleUrls: ['./tree-page.component.scss']
})
export class TreePageComponent implements OnInit {
  public treeId: string;
  public buttonActions: typeof TreeEditorActions;

  constructor(private route: ActivatedRoute) {
    this.buttonActions = TreeEditorActions;
  }

  ngOnInit(): void {
    this.treeId = this.route.snapshot.paramMap.get('id');
  }

}
