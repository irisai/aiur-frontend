import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { SafePipeModule } from '@aiur/core/pipes/safe-pipe';
import { FuseSharedModule } from '@fuse/shared.module';

import { DocumentPanelComponent } from './components/document-panel/document-panel.component';
import { DocumentSearchComponent } from './components/document-search/document-search.component';
import { DocumentOrderComponent } from './components/document-order/document-order.component';
import { DocumentItemComponent } from './components/document-item/document-item.component';
import { DocumentReviewersListComponent } from './components/document-reviewers-list/document-reviewers-list.component';
import { DocumentLoadingComponent } from './components/document-loading/document-loading.component';
import { ContinueReviewButtonComponent } from './components/continue-review-button/continue-review-button.component';
import { ValidateButtonComponent } from './components/validate-button/validate-button.component';
import { EditButtonComponent } from './components/edit-button/edit-button.component';
import { OnScrollDownDirective } from './directives/on-scroll-down.directive';


@NgModule({
  imports: [
    RouterModule,
    MatButtonModule,
    MatIconModule,
    MatProgressSpinnerModule,
    FuseSharedModule,
    SafePipeModule
  ],
  declarations: [
    DocumentPanelComponent,
    DocumentSearchComponent,
    DocumentOrderComponent,
    DocumentItemComponent,
    DocumentLoadingComponent,
    DocumentReviewersListComponent,
    ContinueReviewButtonComponent,
    ValidateButtonComponent,
    EditButtonComponent,
    OnScrollDownDirective
  ],
  exports: [
    DocumentPanelComponent,
    DocumentSearchComponent,
    DocumentOrderComponent,
    DocumentItemComponent,
    DocumentLoadingComponent,
    DocumentReviewersListComponent,
    ContinueReviewButtonComponent,
    ValidateButtonComponent,
    EditButtonComponent
  ]
})
export class DocumentPanelModule {
}
