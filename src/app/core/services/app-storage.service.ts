import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';


const STORAGE_PREFIX = 'aiur.app_';


@Injectable({
  providedIn: 'root'
})
export class AppStorageService {

  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) {
  }

  /**
   * Creates or updates the entry identified by the specified key with the given value.
   * @param key
   * @param value
   */
  set(key: string, value: any): void {
    this.storage.set(this.getStorageKey(key), value);
  }

  /**
   * Retrieves the value stored for the entry that is associated with the specified key.
   * @param key
   */
  get<T>(key: string): T {
    return this.storage.get(this.getStorageKey(key));
  }

  /**
   * Checks whether an entry with the specified key exists in the storage.
   * @param key
   */
  has(key: string): boolean {
    return this.storage.has(this.getStorageKey(key));
  }

  /**
   * Removes the entry that is identified by the specified key.
   * @param key
   */
  remove(key: string): void {
    this.storage.remove(this.getStorageKey(key));
  }

  /**
   * Clears the storage by removing all entries from the storage.
   */
  clear(): void {
    this.storage.clear();
  }

  private getStorageKey(key: string): string {
    return `${STORAGE_PREFIX}${key}`;
  }
}
