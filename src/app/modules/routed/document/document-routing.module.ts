import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TreeViewLayoutComponent } from './components/tree-view-layout/tree-view-layout.component';
import { TreeReviewLayoutComponent } from './components/tree-review-layout/tree-review-layout.component';
import { TreeReviewedLayoutComponent } from './components/tree-reviewed-layout/tree-reviewed-layout.component';
import { TreeValidationLayoutComponent } from './components/tree-validation-layout/tree-validation-layout.component';


const routes: Routes = [
  {path: 'tree/origin', component: TreeViewLayoutComponent},
  {path: 'tree-review/:treeType', component: TreeReviewLayoutComponent},
  {path: 'tree/:treeId/reviewed/:treeType', component: TreeReviewedLayoutComponent},
  {path: 'tree-validation/:treeType', component: TreeValidationLayoutComponent}
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRoutingModule {
}
