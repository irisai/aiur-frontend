import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { TreeEditorActions } from '../models/TreeEditorActions';
import { DndTreeService, IDndTree } from './dnd-tree.service';


@Injectable()
export class TreeEditorService {
  public actions$: Observable<TreeEditorActions>;
  public dndTree: IDndTree;
  private _actionSubject: Subject<TreeEditorActions>;

  constructor(private _dndTree: DndTreeService) {
    this._actionSubject = new Subject<TreeEditorActions>();
    this.actions$ = this._actionSubject.asObservable();
    this.dndTree = this._dndTree.getInstance();
  }

  public emitAction(action: TreeEditorActions): void {
    this._actionSubject.next(action);
  }

}
