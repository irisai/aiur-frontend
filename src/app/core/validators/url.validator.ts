import { AbstractControl } from '@angular/forms';


const URL_REGEXP = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;

export function UrlValidator(control: AbstractControl): { [key: string]: any } | null {
  return URL_REGEXP.test(control.value) ? null : {
    invalidUrl: true
  };
}
