import { Component } from '@angular/core';
import { ErrorsPageCommonService } from '@aiur/modules/routed/errors/services/errors-page-common.service';

@Component({
  selector: 'aif-not-found-page',
  templateUrl: './not-found-page.component.html',
  styleUrls: ['./not-found-page.component.scss']
})
export class NotFoundPageComponent {
  constructor(public errorsPageService: ErrorsPageCommonService) { }
}
