import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { delay, distinctUntilChanged, finalize, retryWhen, scan, shareReplay, tap } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { IUserToken } from '@aiur/core/models/IUser';
import { TokenStorageService } from '@aiur/core/services/token-storage.service';
import { HttpBackendClientService } from '@aiur/core/services/http-backend-client.service';
import { CryptoService } from '@aiur/core/services/crypto.service';
import { env } from '@config/environment';


const LIMIT_TOKEN_REQUEST_COUNT = 3;


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _jwtHelper: JwtHelperService;
  private _clientAuthInProgress: boolean;
  private _userAuthInProgress: boolean;
  private _authSubject: BehaviorSubject<boolean>;


  constructor(private _httpBackend: HttpBackendClientService,
              private _http: HttpClient,
              private _router: Router,
              private _tokenStorage: TokenStorageService,
              private _crypto: CryptoService) {

    this._clientAuthInProgress = false;
    this._userAuthInProgress = false;
    this._jwtHelper = new JwtHelperService();
    this._authSubject = new BehaviorSubject<boolean>(false);
  }

  get isAuthenticated$(): Observable<boolean> {
    const userJWT: string = this._tokenStorage.user().get();
    const refreshJWT: string = this._tokenStorage.refresh().get();

    if (userJWT && !this._jwtHelper.isTokenExpired(userJWT)) {
      this._authSubject.next(true);
    } else if (refreshJWT && !this._jwtHelper.isTokenExpired(refreshJWT)) {
      this._authSubject.next(true);
      this.user().subscribe(
        () => this._authSubject.next(true),
        () => this.logout()
      );
    } else {
      this._authSubject.next(false);
    }

    return this._authSubject.pipe(
      distinctUntilChanged(),
      shareReplay({refCount: true, bufferSize: 1})
    );
  }

  login(email: string, password: string, isRememberMe: boolean): Observable<void> {
    return new Observable<void>((observer) => {
      const {ISODate, encryption} = this._crypto.encrypt(password);

      const url = `${env.API_URL}/api/token/`;
      const body = {
        username: email,
        password: encryption,
        timestamp: ISODate,
        remember_me: isRememberMe
      };

      this._http.post<{ refresh: string, token: string }>(url, body)
        .subscribe(
          ({token, refresh}) => {
            this._tokenStorage.main().create(token);
            this._tokenStorage.user().create(token);
            this._tokenStorage.refresh().create(refresh);
            this._authSubject.next(true);
            observer.next();
          },
          (error) => observer.error(error),
          () => observer.complete()
        );
    });
  }

  logout(): void {
    this._tokenStorage.destroyAll();
    setTimeout(() => this._router.navigate(['auth/login'])
        .then(() => this._authSubject.next(false))
      , 500);
  }

  client(): Observable<string> {
    return new Observable<string>((observer) => {
      let checkAuthorizationIntervalHandler = void (0);

      if (!this._clientAuthInProgress) {
        this._clientAuthInProgress = true;
        const {ISODate, encryption} = this._crypto.encrypt(env.CLIENT_API_KEY);

        const url = `${env.API_URL}/api/token/`;
        const body = {
          timestamp: ISODate,
          client_id: encryption
        };

        this._httpBackend.post<{ token: string }>(url, body)
          .pipe(
            finalize(() => this._clientAuthInProgress = false),
            retryWhen(this._retryRequestToken('client'))
          )
          .subscribe(({token}) => {
              this._tokenStorage.client().create(token);
              observer.next(token);
            },
            (error) => observer.error(error),
            () => observer.complete()
          );
      } else {
        checkAuthorizationIntervalHandler = window.setInterval(() => {
          const clientJWT: string = this._tokenStorage.client().get();

          if (clientJWT && !this._jwtHelper.isTokenExpired(clientJWT)) {
            window.clearInterval(checkAuthorizationIntervalHandler);
            observer.next(clientJWT);
            observer.complete();
          }
        }, 100);
      }
    });
  }

  user(): Observable<string> {
    return new Observable<string>((observer) => {
      let checkAuthorizationIntervalHandler = void (0);

      if (!this._userAuthInProgress) {
        this._userAuthInProgress = true;

        this._getClientToken().pipe(
          tap({next: (clientJWT: string) => this._tokenStorage.main().create(clientJWT)}),
        ).subscribe(
          () => {
            const refreshJWT = this._tokenStorage.refresh().get();

            const url = `${env.API_URL}/api/token/refresh/`;
            const body = {
              refresh: refreshJWT
            };
            const options = {
              withCredentials: true
            };

            this._httpBackend.post<IUserToken>(url, body, options).pipe(
              finalize(() => this._userAuthInProgress = false),
              retryWhen(this._retryRequestToken('user')),
              tap({next: ({token}) => this._tokenStorage.user().create(token)})
            ).subscribe(
              ({token}: IUserToken) => observer.next(token),
              (error) => observer.error(error),
              () => observer.complete()
            )
          },
          (error) => observer.error(error));
      } else {
        checkAuthorizationIntervalHandler = window.setInterval(() => {
          const userJWT = this._tokenStorage.user().get();

          if (userJWT && !this._jwtHelper.isTokenExpired(userJWT)) {
            clearInterval(checkAuthorizationIntervalHandler);
            observer.next(userJWT);
            observer.complete();
          }
        }, 100);
      }
    });
  }

  private _getClientToken(): Observable<string> {
    return new Observable<string>((observer) => {

      const clientJWT = this._tokenStorage.client().get();

      if (clientJWT && !this._jwtHelper.isTokenExpired(clientJWT)) {
        observer.next(clientJWT);
        observer.complete();
      } else {
        this.client().subscribe(
          (clientJWT: string) => observer.next(clientJWT),
          (error) => observer.error(error),
          () => observer.complete(),
        );
      }
    });
  }

  private _retryRequestToken(tokenType: string): (error: Observable<any>) => Observable<any> {
    return (error: Observable<any>): Observable<any> => {
      return error.pipe(
        delay(500),
        scan((errorCount, response) => {
          if (errorCount > LIMIT_TOKEN_REQUEST_COUNT) {
            const customError = Object.assign({}, response, {
              error: {
                type: `EXCEEDED LIMIT TOKEN REQUEST COUNT ERROR`,
                detail: `Exceeded limit of ${tokenType} token requests`
              }
            });

            throw new HttpErrorResponse(customError);
          }

          return errorCount + 1;
        }, 1)
      );
    }
  }
}
