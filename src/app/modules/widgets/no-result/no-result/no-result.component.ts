import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'aif-no-result',
  templateUrl: './no-result.component.html',
  styleUrls: ['./no-result.component.scss'],
  animations: fuseAnimations
})
export class NoResultComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
