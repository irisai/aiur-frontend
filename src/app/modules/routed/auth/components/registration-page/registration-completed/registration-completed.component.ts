import { Component, Input, ViewEncapsulation } from '@angular/core';
import { SafeHtml } from '@angular/platform-browser';
import { fuseAnimations } from '@fuse/animations';
import { FuseConfigService } from '@fuse/services/config.service';


interface ICompleteRegistrationDescription {
  icon: string;
  description: string;
  button: string;
  buttonState: string;
}


@Component({
  selector: 'aif-registration-completed',
  templateUrl: './registration-completed.component.html',
  styleUrls: ['./registration-completed.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class RegistrationCompletedComponent {
  @Input() private isServerError: boolean;
  public completeText: { success: ICompleteRegistrationDescription, error: ICompleteRegistrationDescription };

  constructor(private _fuseConfigService: FuseConfigService) {

    // Configure the layout
    this._fuseConfigService.config = {
      layout: {
        navbar: {hidden: true},
        toolbar: {hidden: true},
        footer: {hidden: true},
        sidepanel: {hidden: true}
      }
    };

    this.completeText = {
      success: {
        icon: 'check_circle',
        // tslint:disable-next-line
        description: `Your account has been created. To complete the subscription process, please click on the link in the email.`,
        button: 'done',
        buttonState: '/auth/login'
      },
      error: {
        icon: 'error_outline',
        // tslint:disable-next-line
        description: `Please try again! If you are here for the second time please contact <a class="link" href="mailto:support@iris.ai">support@iris.ai</a>.`,
        button: 'go back',
        buttonState: '/auth/register'
      }
    };
  }

  getText(key: string): { text: SafeHtml } {
    return {
      text: this.completeText[this.isServerError ? 'error' : 'success'][key]
    };
  }

}
