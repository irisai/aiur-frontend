import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SomethingWentWrongPageComponent } from './something-went-wrong-page.component';

describe('SomethingWentWrongPageComponent', () => {
  let component: SomethingWentWrongPageComponent;
  let fixture: ComponentFixture<SomethingWentWrongPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SomethingWentWrongPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SomethingWentWrongPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
