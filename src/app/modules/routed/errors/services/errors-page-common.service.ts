import { Injectable } from '@angular/core';
import { Location } from '@angular/common';

@Injectable()
export class ErrorsPageCommonService {

  constructor(private location: Location) { }

  previousPage(): void {
    this.location.back();
  }
}
