import { Directive, HostListener, Input } from '@angular/core';
import { TreeEditorActions } from '../models/TreeEditorActions';
import { TreeEditorService } from '../services/tree-editor.service';


@Directive({
  selector: '[aifTreeEditorButton]'
})
export class TreeEditorButtonDirective {
  @Input() public treeEditorButtonAction: TreeEditorActions;

  constructor(private _treeEditor: TreeEditorService) {
  }

  @HostListener('click')
  click(): void {
    this._treeEditor.emitAction(this.treeEditorButtonAction);
  }
}
