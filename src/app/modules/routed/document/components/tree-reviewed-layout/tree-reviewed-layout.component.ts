import { Component, OnInit } from '@angular/core';
import { TreeEditorActions } from '@aiur/modules/widgets/tree-editor';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'aif-reviewed-tree-layout',
  templateUrl: './tree-reviewed-layout.component.html',
  styleUrls: ['./tree-reviewed-layout.component.scss']
})
export class TreeReviewedLayoutComponent implements OnInit {
  public treeId: string;
  public documentId: string;
  public buttonActions: typeof TreeEditorActions;

  constructor(private _route: ActivatedRoute,
              private _router: Router) {

    this.buttonActions = TreeEditorActions;
  }

  ngOnInit(): void {
    this.treeId = this._route.snapshot.paramMap.get('treeId');
    this.documentId = this._route.snapshot.paramMap.get('id');
  }

  isActiveType(type: string): boolean {
    return this._router.url.endsWith(type);
  }
}
