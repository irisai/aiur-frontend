// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  hmr: false,
  VERSION: '1.0.0',
  ENV_NAME: 'local',
  ENV_PREFIX: 'dev_',
  API_URL: 'http://dev-api.projectaiur.com',
  CLIENT_API_KEY: 'Iris_AI_3.0_JxMqdyBdT8O6T4WNmKGCwQdSMTBBI8uP_2017',
  TOKEN: 'fa413f82141adb545c7026b253fd7d78b639789c'
};

export const env = environment;

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
