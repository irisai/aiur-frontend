import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForReviewPageComponent } from './components/for-review-page/for-review-page.component';


const routes: Routes = [
  {path: '', component: ForReviewPageComponent}
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ForReviewRoutingModule {
}
