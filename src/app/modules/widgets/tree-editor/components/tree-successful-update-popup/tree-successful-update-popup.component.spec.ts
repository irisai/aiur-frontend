import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeSuccessfulUpdatePopupComponent } from './tree-successful-update-popup.component';

describe('TreeSuccessfulUpdatePopupComponent', () => {
  let component: TreeSuccessfulUpdatePopupComponent;
  let fixture: ComponentFixture<TreeSuccessfulUpdatePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreeSuccessfulUpdatePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeSuccessfulUpdatePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
