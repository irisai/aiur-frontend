import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentLoadingComponent } from './document-loading.component';

describe('DocumentLoadingComponent', () => {
  let component: DocumentLoadingComponent;
  let fixture: ComponentFixture<DocumentLoadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentLoadingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentLoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
