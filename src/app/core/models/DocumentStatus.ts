export enum DocumentStatus {
  SUBMITTED = 'submitted',
  IN_PROGRESS = 'in-progress',
  UNDER_APPROVAL = 'under-approval',
  INTEGRATED = 'integrated'
}
