export interface IUser {
  readonly user_id: number;
  readonly username: string;
  readonly exp: Date;
  readonly email: string;
  readonly id: number;
  readonly first_name: string;
  readonly position_in_company: string;
  readonly expertises: Array<string>;
  readonly seniority: string;
  readonly type: string;
  readonly image: string;
}

export interface IUserToken {
  readonly token: string;
}

export interface IUserTypes {
  type: string;
  value: string;
}

export interface IUserCreateAccountData {
  readonly first_name: string;
  readonly email: string;
  readonly position_in_company: string;
  readonly expertises: string;
  readonly seniority: string;
  readonly accept_terms: boolean;
}

export interface IUserRegisteredData {
  readonly id: number;
  readonly first_name: string;
  readonly last_name: string;
  readonly send_updates_and_news: boolean;
  readonly username: string;
  readonly email: string;
  readonly is_staff: boolean;
  readonly url: string;
  readonly date_joined: Date;
  readonly last_login: Date;
}

export interface IUserPasswordData {
  readonly password: string;
  readonly password_confirm: string;
}
