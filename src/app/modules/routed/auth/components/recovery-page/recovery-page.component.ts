import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FuseConfigService } from '@fuse/services/config.service';
import { EmailValidator } from '@aiur/core/validators/email.validator';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '@aiur/core/services/user.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpErrorResponse } from '@angular/common/http';


const TEXT = {
  default: {
    title: 'Forgot your password?',
    description: 'Enter your email and we\'ll fix it for you.',
    button: 'REMIND ME'
  },
  expiredLink: {
    title: 'Request new link?',
    description: 'We will use your email to send it to you.',
    button: 'NEW LINK'
  },
  success: {
    title: 'Password reset',
    description: 'We send you an email with password reset link. Please follow the link to continue the password reset process!',
    button: ''
  }
};

@Component({
  selector: 'aif-recovery-page',
  templateUrl: './recovery-page.component.html',
  styleUrls: ['./recovery-page.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class RecoveryPageComponent implements OnInit {
  recoveryForm: FormGroup;
  success: boolean;
  error: boolean;
  recoveryBtnText: string;
  jwtHelper: JwtHelperService;
  textTemplate: any;

  constructor(private _fuseConfigService: FuseConfigService,
              private _formBuilder: FormBuilder,
              private _route: ActivatedRoute,
              private _user: UserService) {

    this.jwtHelper = new JwtHelperService();
    this.success = false;
    this.error = false;
    this.recoveryBtnText = 'Remind me';

    // Configure the layout
    this._fuseConfigService.config = {
      layout: {
        navbar: {hidden: true},
        toolbar: {hidden: true},
        footer: {hidden: true},
        sidepanel: {hidden: true}
      }
    };
  }

  ngOnInit(): void {
    this.recoveryForm = this._formBuilder.group({
      email: ['', [EmailValidator, Validators.required]]
    });

    try {
      const token = this._route.snapshot.queryParams.token;
      const user = typeof token !== 'undefined' && this.jwtHelper.decodeToken(token);
      if (typeof user !== 'undefined' && user.email) {
        this.recoveryForm.patchValue({email: user.email});
        this.form.email.disable();
        this.recoveryBtnText = 'New link';
      }
    } catch (error) {
      // @TODO: Add LoggingService logError function
      console.log(error);
    }
    this.textTemplate = TEXT[this.form.email.disabled ? 'expiredLink' : 'default'];
  }

  get form(): { [key: string]: AbstractControl } {
    return this.recoveryForm.controls;
  }

  recover(): void {
    this._user.resetPassword(this.form.email.value).subscribe(
      () => this._successRecoveryHandler(),
      (error) => this._errorRecoveryHandler(error)
    );
  }

  private _successRecoveryHandler(): void {
    this.textTemplate = TEXT.success;
    this.success = true;
  }

  private _errorRecoveryHandler(response: HttpErrorResponse): void {
    if (response.status === 404 && response.error.status === 'not found') {
      this.recoveryForm.controls['email'].setErrors({unknownUser: true});
    }

    setTimeout(() => this.recoveryForm.controls['email'].setErrors({unknownUser: false}), 3500);
  }
}
