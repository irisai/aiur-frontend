import { NgModule, ErrorHandler } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { HttpInterceptorProviders } from './interceptors';
import { GlobalErrorHandler } from './services/global-error-handler.service';


@NgModule({
  providers: [
    CookieService,
    HttpInterceptorProviders,
    {provide: ErrorHandler, useClass: GlobalErrorHandler}
  ]
})
export class CoreModule {
}
