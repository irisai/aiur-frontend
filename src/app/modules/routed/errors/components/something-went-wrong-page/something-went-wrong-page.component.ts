import { Component } from '@angular/core';
import { ErrorsPageCommonService } from '@aiur/modules/routed/errors/services/errors-page-common.service';

@Component({
  selector: 'aif-something-went-wrong-page',
  templateUrl: './something-went-wrong-page.component.html',
  styleUrls: ['./something-went-wrong-page.component.scss']
})
export class SomethingWentWrongPageComponent{
  constructor(public errorsPageService: ErrorsPageCommonService) { }
}
