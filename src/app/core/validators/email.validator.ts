import { AbstractControl } from '@angular/forms';

const RE = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

export function EmailValidator(control: AbstractControl): { [key: string]: any } | null {
  return (RE.test(control.value.trim()) || control.value.trim().length === 0) ? null : {
    invalidEmail: true
  };
}
