import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeReviewedLayoutComponent } from './tree-reviewed-layout.component';

describe('ReviewedTreeLayoutComponent', () => {
  let component: TreeReviewedLayoutComponent;
  let fixture: ComponentFixture<TreeReviewedLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreeReviewedLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeReviewedLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
