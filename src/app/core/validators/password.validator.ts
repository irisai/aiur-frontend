import { AbstractControl } from '@angular/forms';

const RE = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[ -/:-@[-`{-~])[A-Za-z\d -/:-@[-`{-~]{8,}/;

export function PasswordValidator(control: AbstractControl): { [key: string]: any } | null {
  return RE.test(control.value) ? null : {
    invalidPassword: true
  };
}
