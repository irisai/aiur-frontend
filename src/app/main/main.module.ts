import { NgModule } from '@angular/core';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';
import { LayoutModule } from '@aiur/layout/layout.module';

import { MainRoutingModule } from './main-routing.module';
import { ContainerComponent } from './container/container.component';


@NgModule({
  imports: [
    MainRoutingModule,
    FuseProgressBarModule,
    LayoutModule,
    FuseSharedModule,
    FuseSidebarModule,
    FuseThemeOptionsModule
  ],
  declarations: [
    ContainerComponent
  ],
  exports: [
    ContainerComponent
  ]
})
export class MainModule {
}
