import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from '@aiur/core/models/User';
import { IUserCreateAccountData, IUserTypes, IUserPasswordData, IUserRegisteredData, IUserToken } from '@aiur/core/models/IUser';
import { TokenStorageService } from '@aiur/core/services/token-storage.service';
import { AuthService } from '@aiur/core/services/auth.service';
import { env } from '@config/environment';

export const UserExpertiseInTypesList: Array<IUserTypes> = [
  {type: 'chemistry', value: 'chemistry'},
  {type: 'biology', value: 'biology'},
  {type: 'computer science', value: 'computer science'}
];

export const UserPositionInCompanyTypesList: Array<IUserTypes> = [
  {type: 'researcher', value: 'researcher'},
  {type: 'intern', value: 'intern'},
  {type: 'other', value: 'other'}
];

export const UserSeniorityTypesList: Array<IUserTypes> = [
  {type: 'senior', value: 'senior'},
  {type: 'junior', value: 'junior'}
];

export enum UserUpdateError {
  AUTH_ERROR = 'AUTH_ERROR',
  SERVER_ERROR = 'SERVER_ERROR'
}

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private _jwtHelper: JwtHelperService;

  constructor(private _router: Router,
              private _http: HttpClient,
              private _auth: AuthService,
              private _tokenStorage: TokenStorageService) {

    this._jwtHelper = new JwtHelperService();
  }

  get info$(): Observable<User> {
    return new Observable<User>((observer) => {
      const userJWT: string = this._tokenStorage.user().get();
      const refreshJWT: string = this._tokenStorage.refresh().get();
      if (userJWT && !this._jwtHelper.isTokenExpired(userJWT)) {
        observer.next(new User(this._jwtHelper.decodeToken(userJWT)));
        observer.complete();
      } else if (!userJWT && refreshJWT && !this._jwtHelper.isTokenExpired(refreshJWT)) {
        this._auth.user().subscribe(
          (result: string) => {
            observer.next(new User(this._jwtHelper.decodeToken(result)));
            observer.complete();
          },
          (response) => {
            if (response instanceof HttpErrorResponse && response.error.type === 'EXCEEDED LIMIT TOKEN REQUEST COUNT ERROR') {
              this._tokenStorage.destroyAll();
              this._router.navigate(['auth/login'])
                .then(() => observer.error(new Error('refresh token expired')));
            }
          }
        );
      } else {
        this._router.navigate(['auth/login'])
          .then(() => observer.error(new Error('could not getById user info, token does not exist')));
      }
    });
  }

  registration(user: IUserCreateAccountData): Observable<IUserRegisteredData> {
    const url = `${env.API_URL}/users/`;
    return this._http.post<IUserRegisteredData>(url, user);
  }

  setUpPassword(userId: number, temporaryToken: string, passwordData: IUserPasswordData): Observable<IUserToken> {
    const url = `${env.API_URL}/users/${userId}/`;
    const body = Object.assign({}, {
      system: true,
      old_password: env.TOKEN
    }, passwordData);
    const options = {
      headers: new HttpHeaders({'Authorization': `JWT ${temporaryToken}`})
    };

    return this._http.put<IUserToken>(url, body, options);
  }

  resetPassword(email: string): Observable<{ status: string }> {
    const url = `${env.API_URL}/users/reset-password/`;
    const body = {email};
    return this._http.post<{ status: string }>(url, body);
  }

  updateInfoOnServer(user: User): Observable<any> {
    return new Observable<void>((observer) => {
      this.info$.subscribe((result: User) => {
        const url = `${env.API_URL}/users/${result.id}/`;

        this._http.put<IUserToken>(url, user).subscribe(
          ({token}: IUserToken) => {
            this._updateUserToken(token);
            observer.next();
            observer.complete();
          },
          (response: HttpErrorResponse) => {
            if (401 === response.status && 'Invalid token.' === response.error.detail) {
              // @TODO: compare with previous implementation: update observer.next to observer.error
              observer.next();
            } else if (401 === response.status && 'Password check failed!' === response.error.status) {
              observer.error(UserUpdateError.AUTH_ERROR);
            } else if ((/^5\d{2}/gi).test(response.status.toString())) {
              observer.error(UserUpdateError.SERVER_ERROR);
            } else {
              observer.error(response);
            }
          }
        );
      });
    });
  }

  private _updateUserToken(token: string): void {
    this._tokenStorage.user().create(token);
    this._tokenStorage.main().create(token);
  }
}
