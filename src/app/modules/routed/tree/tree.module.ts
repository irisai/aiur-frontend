import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { TreeRoutingModule } from './tree-routing.module';
import { TreeEditorModule } from '../../widgets/tree-editor';
import { TreePageComponent } from './components/tree-page/tree-page.component';


@NgModule({
  imports: [
    MatButtonModule,
    FuseSharedModule,
    TreeRoutingModule,
    TreeEditorModule
  ],
  declarations: [
    TreePageComponent
  ]
})
export class TreeModule {
}
