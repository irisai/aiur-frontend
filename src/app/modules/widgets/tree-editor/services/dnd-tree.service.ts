import dndTree from '@libs/dndTree/0.0.1/dndTree';
import * as d3 from '@krekhovetskyi/d3-3.5.17-patched';
import { IDependencyTree } from '@aiur/core/models/IDependencyTree';
import { IDndTreeData } from '../models/IDndTreeData';


type DndTreeEvent = 'HE:getData'
  | 'HE:data'
  | 'HE:collapse'
  | 'HE:expand'
  | 'HE:collapseAll'
  | 'HE:expandAll'
  | 'HE:remove'
  | 'HE:dndTree:dragAndDrop'
  | 'HE:sidebar:dragAndDrop'
  | 'HE:alternativeName'
  | 'HE:onLabelClick'
  | 'HE:merge';

interface IDndTreeOptions {
  editable: boolean;
}

export interface IDndTree {
  init(d3instance: typeof d3, options?: IDndTreeOptions): Function;

  setTreeData(data: object): IDndTree;

  getTreeData(): object;

  setParentEl(element: string): IDndTree;

  getParentEl(): HTMLElement;

  event: IEventEmitter
}

interface IEventEmitter {
  subscribe(eventName: DndTreeEvent, callback: Function);

  emit(eventName: DndTreeEvent, data?: any);

  unsubscribeAll(): void;
}

export class DndTreeService {
  private readonly _dndTree: IDndTree;

  constructor() {
    this._dndTree = dndTree;
  }

  getInstance(): IDndTree {
    return this._dndTree;
  }

  generateDndTreeDataStructure(data: IDependencyTree): IDndTreeData {
    const {title, year, abstract, download_url, fingerprint, fulltext_link, identifier, journal, children, metadata, ord_id} = data;

    return {
      id: ord_id,
      depth: 0,
      isMouseOver: false,
      isHighlighted: false,
      name: title,
      metadata, title, year, abstract, download_url, fingerprint, fulltext_link, identifier, journal, ord_id,
      children: this._dndTreeGenerator(children, 1),
      _children: null
    };
  }

  generateDependencyTreeDataStructure(data: Partial<IDndTreeData>): IDependencyTree {
    const {title, year, identifier, journal, fulltext_link, download_url, abstract, fingerprint, name, metadata, children, ord_id} = data;

    return {
      title, year, identifier, journal, fulltext_link, download_url, abstract, fingerprint, name, metadata, ord_id,
      children: _dependencyTreeGenerator(children)
    };

    function _dependencyTreeGenerator(dataList: Array<Partial<IDndTreeData>> = []): Array<IDependencyTree> {
      return dataList.map((item: Partial<IDndTreeData>) => {
        const {title, year, identifier, journal, fulltext_link, download_url, abstract, fingerprint, name, metadata, children, _children, ord_id} = item;

        const child: Array<Partial<IDndTreeData>> = (children && children.length)
          ? children
          : ((_children && _children.length) ? _children : []);

        return {
          title, year, identifier, journal, fulltext_link, download_url, abstract, fingerprint, name, metadata, ord_id,
          children: [].concat(child.length ? _dependencyTreeGenerator(child) : [])
        };
      });
    }
  }

  private _dndTreeGenerator(childrenList: Array<IDependencyTree> = [], depth: number): Array<IDndTreeData> {
    return childrenList.map(({
                               title,
                               name,
                               year,
                               abstract,
                               download_url,
                               fingerprint,
                               fulltext_link,
                               identifier,
                               journal,
                               children,
                               metadata,
                               ord_id
                             }) => {

      console.log('-----------------');
      console.log('ord_id: ', ord_id);
      console.log('name: ', name);


      return {
        id: ord_id,
        isMouseOver: false,
        isHighlighted: false,
        metadata, depth, title, name, year, abstract, download_url, fingerprint, fulltext_link, identifier, journal, ord_id,
        children: Object.keys(children).length ? this._dndTreeGenerator(children, (depth + 1)) : [],
        _children: [],
      };
    });
  }
}
