import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentReviewersListComponent } from './document-reviewers-list.component';

describe('DocumentReviewersListComponent', () => {
  let component: DocumentReviewersListComponent;
  let fixture: ComponentFixture<DocumentReviewersListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentReviewersListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentReviewersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
