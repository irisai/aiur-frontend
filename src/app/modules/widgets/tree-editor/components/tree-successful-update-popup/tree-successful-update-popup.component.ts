import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';


@Component({
  selector: 'aif-tree-successful-update-popup',
  templateUrl: './tree-successful-update-popup.component.html',
  styleUrls: ['./tree-successful-update-popup.component.scss']
})
export class TreeSuccessfulUpdatePopupComponent {

  constructor(private _dialogRef: MatDialogRef<TreeSuccessfulUpdatePopupComponent>) {
  }

  confirm() {
    this._dialogRef.close();
  }
}
