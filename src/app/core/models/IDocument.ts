import { IDependencyTreeMetadata } from './IDependencyTreeMetadata';
import { DocumentStatus } from './DocumentStatus';


export interface IDocument {
  id: number;
  document_identifier: string;
  title: string;
  abstract: string;
  authors: string | null;
  year: number;
  tags: Array<string> | null;
  status: DocumentStatus;
  fulltext_link: string;
  owner_id: string;
  short_name: string;
  trees: Array<IDependencyTreeMetadata>;
  created_at: Date;
  modified_at: Date;
}

export enum DocumentsOrdering {
  NEWEST = '-created_at',
  OLDEST = 'created_at'
}
