import { Component, Input } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
import { IDocument } from '@aiur/core/models/IDocument';


@Component({
  selector: 'aif-document-item',
  templateUrl: './document-item.component.html',
  styleUrls: ['./document-item.component.scss'],
  animations: [
    trigger('myInsertRemoveTrigger', [
      transition(':enter', [
        style({opacity: 0, height: 0}),
        animate('0.35s', style({opacity: 1, height: '100%'})),
      ]),
      transition(':leave', [
        animate('0.35s', style({opacity: 0, height: 0}))
      ])
    ]),
  ]
})
export class DocumentItemComponent {
  @Input() public document: IDocument;
  public isDetailShown: boolean;

  constructor() {
    this.isDetailShown = false;
  }

  showDetail(): void {
    this.isDetailShown = !this.isDetailShown;
  }
}
