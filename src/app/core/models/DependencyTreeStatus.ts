export enum DependencyTreeStatus {
  SUBMITTED = 'submitted',
  IN_PROGRESS = 'in-progress',
  APPROVED = 'approved',
}
