import { TestBed } from '@angular/core/testing';

import { DndTreeService } from './dnd-tree.service';

describe('DndTreeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DndTreeService = TestBed.get(DndTreeService);
    expect(service).toBeTruthy();
  });
});
