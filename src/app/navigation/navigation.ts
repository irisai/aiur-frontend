import { FuseNavigation } from '@fuse/types';


export const navigation: FuseNavigation[] = [
  {
    id: 'applications',
    title: 'Graph of knowledge',
    type: 'group',
    children: [
      {
        id: 'explore',
        title: 'Explore',
        type: 'item',
        icon: 'explore',
        url: '/explore'
      },
      {
        id: 'myTrees',
        title: 'My trees',
        type: 'item',
        icon: 'device_hub',
        url: '/my-trees'
      },
      {
        id: 'myReviews',
        title: 'My reviews',
        type: 'item',
        icon: 'send',
        url: '/my-reviews'
      },
      {
        id: 'forReview',
        title: 'For review',
        type: 'item',
        icon: 'loop',
        url: '/for-review'
      }
    ]
  }
];
