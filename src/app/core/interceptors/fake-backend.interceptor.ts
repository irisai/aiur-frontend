import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, dematerialize, materialize, mergeMap } from 'rxjs/operators';
import { treeDataExample } from '@aiur/core/data/original-tree-data';
import { reviewedTreeDataExample } from '@aiur/core/data/reviewed-tree-data';
import { treeListDataExample } from '@aiur/core/data/tree-list-data';


const users: Array<{ username: string }> = [{username: 'test@test.com'}];


export class FakeBackendInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const {url, method, body} = request;

    return of(null)
      .pipe(
        mergeMap(handleRoute),
        materialize(),
        delay(800),
        dematerialize()
      );

    function handleRoute(): Observable<HttpEvent<any>> {

      switch (true) {
        // case url.endsWith('/api/token/') && method === 'POST':
        //   return apiToken();

        // case url.endsWith('/research/submission') && method === 'POST':
        //   return researchSubmission();

        // case url.includes('/tree/') && method === 'GET':
        //   return treeData();

        // case url.includes('/tree-list/') && method === 'GET':
        //   return treeListData();

        // case url.endsWith('/users/reset-password/') && method === 'POST':
        //   return registration();

        // case url.endsWith('/users/') && method === 'POST':
        //   return registration();

        default:
          // pass through any requests not handled above
          return next.handle(request);
      }
    }

    function apiToken(): Observable<any> {
      const {username} = body;

      if (username === users[0].username) {
        return ok(Object.assign({}, {
          refresh: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0eXBlIjoicmVmcmVzaCIsInVzZXJfaWQiOjIyMzAyLCJleHAiOjE1Nzc4MzY4MDB9.PWlacHcNcTnZ07lr8lay0k6IOZQf669jA66aI25_CBk',
          token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InRlc3RAdGVzdC5jb20iLCJmaXJzdF9uYW1lIjoiQWxiZXJ0IiwibGFzdF9uYW1lIjoiRWluc3RlaW4iLCJpbWFnZSI6ImRhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBR1FBQUFCa0NBSUFBQUQvZ0FJREFBQUJTMGxFUVZSNG5PM2J3VTNEUUJCQTBRUnhvUzlhb1NwYW9TNXpwUUlMZjlacjF0Sjc5eWlUci9WaHRNNXoyN1lIeDd6ODl3QjNJbFlnVmlCV0lGWWdWaUJXOERyeTRZK3Z0N1BtK0h6L1h1ZTc5amhaZ1ZpQldJRllnVmlCV0lGWWdWaUJXSUZZZ1ZpQldNSFFJbjFrSXoxeEFUN2l6MHZ5RVU1V0lGWWdWaUJXSUZZZ1ZpQldJRllnVmlCV3NMdnVIRnhUcHE0WE00ejhMaWNyRUNzUUt4QXJFQ3NRS3hBckVDc1FLeEFyRUNzWXVncDdYSDdUOWF1cDh6eEgvdWkwMnJJOWV4NlBZU0JXSUZZZ1ZpQldJRllnVmlCV0lGWWdWakM2RzU1aXRiVnB6L1IzU3E4MGV4NlBZU0JXSUZZZ1ZpQldJRllnVmlCV0lGWWdWbkRGVmRocVhJVmRRYXhBckVDc1FLeEFyRUNzUUt4QXJFQ3NRS3hBckdCb2tUN0xYUzVabmF4QXJFQ3NRS3hBckVDc1FLeEFyRUNzUUt4QXJHQ0ozZkF1bkt4QXJFQ3NRS3hBckVDczRBZURHalVRZDFGSlZnQUFBQUJKUlU1RXJrSmdnZz09IiwiZXhwIjoxNTc3ODM2ODAwLCJlbWFpbCI6InVzZXJAaXQtc2ltcGxlLm5ldCIsImlkIjoxLCJ1c2VyX3R5cGUiOiJzY2llbmNlX2VudGh1c2lhc3QiLCJ1cmwiOiJodHRwOi8vZGV2LWFwaS5wcm9qZWN0YWl1ci5jb20vdXNlcnMvMS8iLCJwb3NpdGlvbl9pbl9jb21wYW55IjoiT3RoZXIiLCJleHBlcnRpc2VzIjpbIkNoZW1pc3RyeSIsIkJpb2xvZ3kiXSwic2VuaW9yaXR5IjoiU2VuaW9yIn0.IJy2JC1hjTgc8I-QB3rSrBX282_v3Z78TBEADdz119U'
        }));
      } else {
        return next.handle(request);
      }
    }

    function researchSubmission(): Observable<any> {
      return ok({status: 'ok'});
    }

    function treeData(): Observable<any> {
      return ok(treeDataExample);
    }

    function treeListData(): Observable<any> {
      return ok(treeListDataExample);
    }

    function registration(): Observable<any> {
      return ok({
        id: 1,
        user_id: 1,
        username: 'test@test.com',
        first_name: 'Albert',
        last_name: 'Einstein',
        email: 'user@it-simple.net',
        user_type: 'science_enthusiast'
      });
    }

    function resetPassword(): Observable<any> {
      return ok({status: 'ok'});
    }

    function ok(body?: any): Observable<any> {
      return of(new HttpResponse({status: 200, body}));
    }

    function error(message): Observable<any> {
      return throwError({error: {message}});
    }
  }
}
