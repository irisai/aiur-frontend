import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeViewLayoutComponent } from './tree-view-layout.component';

describe('TreeViewLayoutComponent', () => {
  let component: TreeViewLayoutComponent;
  let fixture: ComponentFixture<TreeViewLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreeViewLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeViewLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
