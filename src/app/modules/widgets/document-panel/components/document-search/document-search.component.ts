import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';


@Component({
  selector: 'aif-document-search',
  templateUrl: './document-search.component.html',
  styleUrls: ['./document-search.component.scss']
})
export class DocumentSearchComponent implements OnInit, OnDestroy {
  @Output() public searchUpdated: EventEmitter<string>;
  public searchForm: FormGroup;
  public queryInputChangesSubscription: Subscription;

  constructor(private _formBuilder: FormBuilder) {
    this.searchUpdated = new EventEmitter<string>();
  }

  ngOnInit(): void {
    this.searchForm = this._formBuilder.group({
      query: ['']
    });

    this.queryInputChangesSubscription = this.searchForm.controls.query.valueChanges
      .pipe(debounceTime(500))
      .subscribe((query: string) => this.searchUpdated.emit(query));
  }

  ngOnDestroy(): void {
    this.queryInputChangesSubscription.unsubscribe();
  }
}
