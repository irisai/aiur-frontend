import { IDependencyTree } from './IDependencyTree';
import { DependencyTreeType } from './DependencyTreeType';
import { DependencyTreeStatus } from './DependencyTreeStatus';


export interface IDependencyTreeMetadata {
  id: number;
  type: DependencyTreeType;
  document_id: number;
  status: DependencyTreeStatus;
  owner: {
    id: number;
    first_name: string;
  };
  owner_id: number;
  dependency_tree: IDependencyTree;
  created_at: Date;
  modified_at: Date;
}
