import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule, MatSelectModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { RegistrationPageComponent } from './components/registration-page/registration-page.component';
import { RegistrationCompletedComponent } from './components/registration-page/registration-completed/registration-completed.component';
import { SafePipeModule } from '@aiur/core/pipes/safe-pipe';
import { SetupPasswordPageComponent } from './components/setup-password-page/setup-password-page.component';
import { RecoveryPageComponent } from './components/recovery-page/recovery-page.component';


@NgModule({
  imports: [
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    FuseSharedModule,
    AuthRoutingModule,
    SafePipeModule
  ],
  declarations: [
    LoginPageComponent,
    RegistrationPageComponent,
    RegistrationCompletedComponent,
    SetupPasswordPageComponent,
    RecoveryPageComponent
  ]
})
export class AuthModule {
}
