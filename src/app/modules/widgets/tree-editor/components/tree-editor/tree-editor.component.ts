import { AfterViewInit, Component, Input, NgZone, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import * as d3 from '@krekhovetskyi/d3-3.5.17-patched';
import { TreeEditorService } from '../../services/tree-editor.service';
import { IDndTreeData } from '../../models/IDndTreeData';


@Component({
  selector: 'aif-tree-editor',
  template: ``,
  styleUrls: ['./tree-editor.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TreeEditorComponent implements AfterViewInit, OnInit, OnDestroy {
  @Input() public treeData: IDndTreeData;
  @Input() public edit: boolean;
  private _dndTreeDestroy: Function;
  private _isTreeEditable: boolean;

  constructor(private _zone: NgZone,
              private _treeEditor: TreeEditorService) {
  }

  ngOnInit(): void {
    this._isTreeEditable = Boolean(this.edit);
  }

  ngAfterViewInit(): void {
    this._zone.runOutsideAngular(() => {
      this._dndTreeDestroy = this._treeEditor
        .dndTree
        .setParentEl('aif-tree-editor')
        .setTreeData(this.treeData)
        .init(d3, {editable: this._isTreeEditable});
    });
  }

  ngOnDestroy(): void {
    this._dndTreeDestroy();
  }

}
