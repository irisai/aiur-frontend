import { Component } from '@angular/core';

@Component({
  selector: 'aif-document-loading',
  template: `
    <mat-spinner diameter="50" fxFlexAlign="center"></mat-spinner>
  `,
  styles: [`
    :host {
      display: flex;
      overflow: hidden;
    }

    mat-spinner {
      margin: 0 auto;
    }
  `]
})
export class DocumentLoadingComponent {
}
