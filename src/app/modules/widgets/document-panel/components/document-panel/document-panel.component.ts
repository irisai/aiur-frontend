import { Component } from '@angular/core';
import { DocumentsService } from '@aiur/core/services/documents.service';


@Component({
  selector: 'aif-document-panel',
  templateUrl: './document-panel.component.html',
  styleUrls: ['./document-panel.component.scss']
})
export class DocumentPanelComponent {
  constructor(private _documents: DocumentsService) {

  }

  onScroll(): void {
    this._documents.load();
  }
}
