import { NgModule } from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatStepperModule } from '@angular/material/stepper';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FuseSharedModule } from '@fuse/shared.module';
import { ExploreRoutingModule } from './explore-routing.module';
import { ExplorePageComponent } from './components/explore-page/explore-page.component';


@NgModule({
  imports: [
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatButtonToggleModule,
    MatFormFieldModule,
    MatStepperModule,
    FuseSharedModule,
    ExploreRoutingModule
  ],
  declarations: [
    ExplorePageComponent
  ]
})
export class ExploreModule {
}
