import { Injectable } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { CookieService } from 'ngx-cookie-service';


@Injectable({
  providedIn: 'root'
})
export class CookiesStorageService {
  private readonly _PATH: string;
  private readonly _DOMAIN: string;
  private readonly _SECURE: boolean;
  private readonly _FIVE_DAYS: number;


  constructor(private _cookiesService: CookieService,
              private _platformLocation: PlatformLocation) {

    this._PATH = '/';
    this._DOMAIN = '.projectaiur.com';
    this._SECURE = 'https' === (this._platformLocation['location'] as Location).protocol;
    this._FIVE_DAYS = 432000000;
  }

  /**
   * @param name Cookie name
   * @returns {any}
   */
  get(name: string): string {
    return this._cookiesService.get(name);
  }

  /**
   * @param name     Cookie name
   * @param value    Cookie value
   * @param expires  Number of days until the cookies expires or an actual `Date`
   */
  set(name: string, value: string, expires: Date = new Date(Date.now() + this._FIVE_DAYS)): void {
    this._cookiesService.set(name, value, expires, this._PATH, this._DOMAIN, this._SECURE, 'Strict')
  }

  /**
   * @param name   Cookie name
   */
  delete(name: string): void {
    this._cookiesService.delete(name, this._PATH, this._DOMAIN);
  }
}
