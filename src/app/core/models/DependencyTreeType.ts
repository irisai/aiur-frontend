export enum DependencyTreeType {
  REVIEW = 'review',
  SUBMISSION = 'submission'
}
