import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { MyTreesRoutingModule } from './my-trees-routing.module';
import { MyTreesPageComponent } from './components/my-trees-page/my-trees-page.component';
import { DocumentPanelModule } from '@aiur/modules/widgets/document-panel/document-panel.module';
import { NoResultModule } from '@aiur/modules/widgets/no-result/no-result.module';


@NgModule({
  imports: [
    FuseSharedModule,
    MyTreesRoutingModule,
    DocumentPanelModule,
    NoResultModule
  ],
  declarations: [MyTreesPageComponent]
})
export class MyTreesModule {
}
